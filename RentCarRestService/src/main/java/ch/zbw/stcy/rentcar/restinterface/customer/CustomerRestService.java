package ch.zbw.stcy.rentcar.restinterface.customer;

import ch.zbw.stcy.rentcar.businesslogic.BoundaryFactory;
import ch.zbw.stcy.rentcar.businesslogic.customer.Customer;
import ch.zbw.stcy.rentcar.businesslogic.customer.ICustomerBoundary;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Collection;
import java.util.NoSuchElementException;

/**
 * Created by cyrill on 26.05.17.
 */


@Path("customers")
public class CustomerRestService {

    private ICustomerBoundary customerBoundary;
    private final String ENCODING = "UTF_8";

    @Inject
    private void setBoundary() {
        customerBoundary = BoundaryFactory.createCustomerBoundary();
    }


    //http://localhost:8080/rentcar/services/customers
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllCustomers() {

        Collection<Customer> allCustomers = customerBoundary.getAllCustomers();
        GenericEntity<Collection<Customer>> genericEntity = getCollectionGenericEntity(allCustomers);

        return Response.ok()
                .encoding(ENCODING)
                .entity(genericEntity)
                .build();
    }

    //http://localhost:8080/rentcar/services/customers/100001

    @GET
    @Path("{nr}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerByNr(@PathParam("nr") int nr) throws WebApplicationException {
        Customer customer = customerBoundary.getCustomerByNr(nr)
                .orElseThrow(() -> new NoSuchElementException("customer not in list"));
        return Response.ok()
                .encoding(ENCODING)
                .entity(customer)
                .build();
    }
    //http://localhost:8080/rentcar/services/customers/searchString
    @GET
    @Path("searchByString")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomersBySearchString(@DefaultValue("") @QueryParam("searchString") String search){
        Collection<Customer> customers = customerBoundary.searchCustomers(search);
        GenericEntity<Collection<Customer>> collectionGenericEntity = getCollectionGenericEntity(customers);
        return Response.ok()
                .encoding(ENCODING)
                .entity(collectionGenericEntity)
                .build();
    }

    //http://localhost:8080/rentcar/services/customers/100001

    @DELETE
    @Path("{nr}")
    public Response deleteCustomerByNr(@PathParam("nr") int nr) {
        customerBoundary.getCustomerByNr(nr).orElseThrow(NoSuchElementException::new);

        boolean deleted = customerBoundary.deleteCustomer(nr);
        if (deleted) {
            return Response.ok().build();
        } else {
            return Response.serverError().build();
        }
    }
    //http://localhost:8080/rentcar/services/customers/

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveCustomer(Customer customer) {
        Customer savedCustomer = customerBoundary.saveCustomer(customer);
        final URI location = UriBuilder.fromResource(CustomerRestService.class)
                .path(Long.toString(savedCustomer.getNumber())).build();
        return Response.created(location)
                .build();
    }
    //http://localhost:8080/rentcar/services/customers/100001

    @PUT
    @Path("{nr}")
    @Consumes({MediaType.APPLICATION_JSON +  ";charset=UTF-8"})
    public Response updateCustomer(@PathParam("nr") int nr, Customer customer) {
        if (customer == null) throw new IllegalArgumentException("customer null not allowed");

        long number = customer.getNumber();
        if (number != 0 && number != nr) throw new IllegalArgumentException("number is different to customer.nr");

        Customer custInStore = customerBoundary.getCustomerByNr(nr)
                .orElseThrow(() -> new NoSuchElementException("customer with this nr not in list"));
        custInStore.update(customer);
        customerBoundary.updateCustomer(custInStore);
        return Response.ok().build();
    }

    private GenericEntity<Collection<Customer>> getCollectionGenericEntity(Collection<Customer> customerCollection) {
        return new GenericEntity<Collection<Customer>>(customerCollection){};
    }
}
