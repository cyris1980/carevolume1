package ch.zbw.stcy.rentcar;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by cyrill on 07.07.17.
 */
@ApplicationPath("services")
public class RentCarApplication extends Application {
}
