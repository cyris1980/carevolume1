package ch.zbw.stcy.rentcar.restinterface.car;

import ch.zbw.stcy.rentcar.businesslogic.BoundaryFactory;
import ch.zbw.stcy.rentcar.businesslogic.car.ICarBoundary;
import ch.zbw.stcy.rentcar.businesslogic.car.Car;
import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClass;
import ch.zbw.stcy.rentcar.businesslogic.carClass.ICarClassBoundary;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

/**
 * Created by stefanallenspach on 02.08.17.
 */

@Path("car")
public class CarRestService {

    private ICarBoundary carBoundary;
    private ICarClassBoundary carClassBoundary;

    private static final String ENCODING = "UTF_8";

    @Inject
    private void setBondary() {
        carBoundary = BoundaryFactory.createCarBoundary();
        carClassBoundary = BoundaryFactory.createCarClassBoundary();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllCars() {
        Collection<CarTransport> allCars = carBoundary.getAllCars().stream()
                .map(CarTransport::new)
                .collect(Collectors.toList());
        GenericEntity<Collection<CarTransport>> genericEntity = getCollectionGenericEntity(allCars);

        return Response.ok()
                .encoding(ENCODING)
                .entity(genericEntity)
                .build();
    }


    //http://localhost:8080/rentcar/services/car/CR-0001
    @GET
    @Path("{identification}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCarByIdentification(@PathParam("identification") String identification) throws WebApplicationException {
        Car car = carBoundary.getCarByIdentification(identification)
                .orElseThrow(() -> new NoSuchElementException("car not in list"));

        CarTransport carTransport = new CarTransport(car);

        return Response.ok()
                .encoding(ENCODING)
                .entity(carTransport)
                .build();
    }

    //http://localhost:8080/rentcar/services/car/carsByCarClassName?carClassName=Mittelklasse
    @GET
    @Path("carsByCarClassName")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCarsByCarClassName(@DefaultValue("") @QueryParam("carClassName") String carClassName) throws WebApplicationException {

        CarClass carClass = getCarClass(carClassName);

        Collection<CarTransport> allCars = carBoundary.getAllCarsByCarClass(carClass).stream()
                .map(CarTransport::new)
                .collect(Collectors.toList());

        GenericEntity<Collection<CarTransport>> genericEntity = getCollectionGenericEntity(allCars);

        return Response.ok()
                .encoding(ENCODING)
                .entity(genericEntity)
                .build();
    }

    //http://localhost:8080/rentcar/services/car/CR-0001
    @DELETE
    @Path("{identification}")
    public Response deleteCarByIdentification(@PathParam("identification") String identification) {
        boolean deleted = carBoundary.deleteCarByIdentification(identification);
        if (deleted) {
            return Response.ok().build();
        } else {
            throw new NoSuchElementException("car not in list");
        }
    }


    //http://localhost:8080/rentcar/services/car/
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveCar(CarTransport carTransport) {
        Car car = getCar(carTransport);
        carBoundary.saveCar(car);
        return Response.status(Response.Status.CREATED).build();
    }


    //http://localhost:8080/rentcar/services/car/CR-0001
    @PUT
    @Path("{identification}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCar(@PathParam("identification") String identification, CarTransport carTransport) {

        Car carOriginal = carBoundary.getCarByIdentification(identification)
                .orElseThrow(() -> new IllegalArgumentException("car not in list"));

        Car carNew = getCar(carTransport);

        carOriginal.update(carNew);
        carBoundary.updateCar(carOriginal);

        return Response.ok().build();
    }


    private Car getCar(CarTransport carTransport) {
        String carClassName = carTransport.getCarClassName();
        String identification = carTransport.getIdentification();
        String brand = carTransport.getBrand();
        String typ = carTransport.getTyp();
        CarClass carClass = getCarClass(carClassName);
        return new Car(identification, brand, typ, carClass);
    }

    private CarClass getCarClass(String carClassName) {
        return carClassBoundary.getCarClassByName(carClassName)
                    .orElseThrow(() -> new IllegalArgumentException("CarClass not in list"));
    }

    private GenericEntity<Collection<CarTransport>> getCollectionGenericEntity(Collection<CarTransport> carCollection) {
        return new GenericEntity<Collection<CarTransport>>(carCollection) {
        };
    }

}
