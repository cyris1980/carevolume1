package ch.zbw.stcy.rentcar.restinterface.car;

import ch.zbw.stcy.rentcar.businesslogic.car.Car;

/**
 * Created by stefanallenspach on 02.08.17.
 */
public class CarTransport {

    private String identification;
    private String brand;
    private String typ;
    private String carClassName;

    public CarTransport() {
    }

    public CarTransport(Car car) {
        this.identification = car.getIdentification();
        this.typ = car.getTyp();
        this.brand = car.getBrand();
        this.carClassName = car.getCarClass().getName();
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getCarClassName() {
        return carClassName;
    }

    public void setCarClassName(String carClassName) {
        this.carClassName = carClassName;
    }
}
