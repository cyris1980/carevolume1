package ch.zbw.stcy.rentcar.restinterface.carclass;

import ch.zbw.stcy.rentcar.businesslogic.BoundaryFactory;
import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClass;
import ch.zbw.stcy.rentcar.businesslogic.carClass.ICarClassBoundary;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Collection;
import java.util.NoSuchElementException;

/**
 * Created by stefanallenspach on 26.07.17.
 */

@Path("carClass")
public class CarClassRestService {

    private ICarClassBoundary carClassBoundary;
    private final String ENCODING = "UTF_8";

    @Inject
    private void setBondary() {
        carClassBoundary = BoundaryFactory.createCarClassBoundary();
    }

    //http://localhost:8080/rentcar/services/carClass
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllCarClasses() {
        Collection<CarClass> allCarClasses = carClassBoundary.getAllCarClasses();
        GenericEntity<Collection<CarClass>> genericEntity = getCollectionGenericEntity(allCarClasses);

        return Response.ok()
                .encoding(ENCODING)
                .entity(genericEntity)
                .build();
    }

    //http://localhost:8080/rentcar/services/carClass/Mittelklasse
    @GET
    @Path("{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCarClassByName(@PathParam("name") String name) throws WebApplicationException {
        CarClass carClass = carClassBoundary.getCarClassByName(name)
                .orElseThrow(() -> new NoSuchElementException("carClass not in list"));

        return Response.ok()
                .encoding(ENCODING)
                .entity(carClass)
                .build();
    }



    //http://localhost:8080/rentcar/services/carClass/Luxusklasse
    @DELETE
    @Path("{name}")
    public Response deleteCarClassByName(@PathParam("name") String name) {
        boolean deleted = carClassBoundary.deleteCarClassByName(name);
        if (deleted) {
            return Response.ok().build();
        } else {
            throw new NoSuchElementException("carClass not in list");
        }
    }


    //http://localhost:8080/rentcar/services/carClass/
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveCarClass(CarClass carClass) {
        CarClass savedCarClass = carClassBoundary.saveCarClass(carClass);
        final URI location = UriBuilder.fromResource(CarClassRestService.class)
                .path(savedCarClass.getName()).build();
        return Response.created(location).build();
    }

    //http://localhost:8080/rentcar/services/carClass/Luxusklasse
    @PUT
    @Path("{name}")
    @Consumes({MediaType.APPLICATION_JSON + ";charset=UTF-8"})
    public Response updateCarClass(@PathParam("name") String name,CarClass carClass) {
        String carClassName = carClass.getName();
        if(!carClassName.equals(name)) throw new IllegalArgumentException("name is different to carClass.name");

        carClassBoundary.updateCarClass(carClass);
        return Response.ok().build();
    }

    private GenericEntity<Collection<CarClass>> getCollectionGenericEntity(Collection<CarClass> carClassCollection) {
        return new GenericEntity<Collection<CarClass>>(carClassCollection) {
        };
    }

}
