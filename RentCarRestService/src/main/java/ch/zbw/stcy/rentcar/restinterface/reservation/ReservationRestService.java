package ch.zbw.stcy.rentcar.restinterface.reservation;

import ch.zbw.stcy.rentcar.businesslogic.BoundaryFactory;
import ch.zbw.stcy.rentcar.businesslogic.car.ICarBoundary;
import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClass;
import ch.zbw.stcy.rentcar.businesslogic.carClass.ICarClassBoundary;
import ch.zbw.stcy.rentcar.businesslogic.customer.Customer;
import ch.zbw.stcy.rentcar.businesslogic.customer.ICustomerBoundary;
import ch.zbw.stcy.rentcar.businesslogic.reservation.IReservationBoundary;
import ch.zbw.stcy.rentcar.businesslogic.reservation.Reservation;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

/**
 * Created by stefanallenspach on 26.07.17.
 */

@Path("reservation")
public class ReservationRestService {

    private IReservationBoundary reservationBoundary;
    private ICustomerBoundary customerBoundary;
    private ICarClassBoundary carClassBoundary;
    private ICarBoundary carBoundary;
    private final String ENCODING = "UTF_8";

    @Inject
    private void setBoundary() {
        reservationBoundary = BoundaryFactory.createReservationBoundary();
        customerBoundary = BoundaryFactory.createCustomerBoundary();
        carClassBoundary = BoundaryFactory.createCarClassBoundary();
        carBoundary = BoundaryFactory.createCarBoundary();
    }

    //http://localhost:8080/rentcar/services/reservation
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllReservations() {
        Collection<ReservationTransport> allReservations = reservationBoundary.getAllReservation().stream()
                .map(ReservationTransport::new)
                .collect(Collectors.toList());
        GenericEntity<Collection<ReservationTransport>> genericEntity = getCollectionGenericEntity(allReservations);

        return Response.ok()
                .encoding(ENCODING)
                .entity(genericEntity)
                .build();
    }

    //http://localhost:8080/rentcar/services/reservation
    @GET
    @Path("{number}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReservationByNumber(@PathParam("number") long number) {
        Reservation reservation = reservationBoundary.getReservationByNr(number)
                .orElseThrow(() -> new NoSuchElementException("reservation not in list"));

        ReservationTransport reservationTransport = new ReservationTransport(reservation);

        return Response.ok()
                .encoding(ENCODING)
                .entity(reservationTransport)
                .build();
    }

    //http://localhost:8080/rentcar/services/reservation/searchByCustomerSearchString?customerSearchString=LL
    @GET
    @Path("searchByCustomerSearchString")
    public Response getReservationByCustomerSearchString(@DefaultValue("") @QueryParam("customerSearchString") String search) {
        Collection<Customer> customers = customerBoundary.searchCustomers(search);
        Collection<ReservationTransport> totalReservations = new ArrayList<>();

        for (Customer customer : customers) {
            Collection<ReservationTransport> allReservations = reservationBoundary.getAllReservationFromCustomer(customer).stream()
                    .map(ReservationTransport::new)
                    .collect(Collectors.toList());
            totalReservations.addAll(allReservations);
        }
        GenericEntity<Collection<ReservationTransport>> collectionGenericEntity = getCollectionGenericEntity(totalReservations);
        return Response.ok()
                .encoding(ENCODING)
                .entity(collectionGenericEntity)
                .build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveReservation(ReservationTransport reservationTransport) {

        if (reservationTransport.getReservationNo() > 0)
            throw new IllegalArgumentException("Save Reservation with a reservationNumber is not allowed (Setting by System)");

        Customer customer = customerBoundary.getCustomerByNr(reservationTransport.getCustomerNo())
                .orElseThrow(() -> new IllegalArgumentException("Customer not in list"));

        CarClass carClass = carClassBoundary.getCarClassByName(reservationTransport.getCarClassName())
                .orElseThrow(() -> new IllegalArgumentException("CarClass not in list"));

        LocalDate startDate = LocalDate.parse(reservationTransport.getStartDate());
        LocalDate endDate = LocalDate.parse(reservationTransport.getEndDate());
        Reservation reservation = new Reservation(customer, startDate, endDate, carClass);
        reservation = reservationBoundary.saveReservation(reservation);

        final URI location = UriBuilder.fromResource(ReservationRestService.class)
                .path(Long.toString(reservation.getReservationNumber())).build();
        return Response.created(location).build();
    }


    @PUT
    @Path("{number}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCarClass(@PathParam("number") long number, ReservationTransport reservationTransport) {

        Reservation reservationOriginal = reservationBoundary.getReservationByNr(number).orElseThrow(() -> new IllegalArgumentException("customer not in list"));
        Reservation reservationNew = reservationTransport.getReservation(customerBoundary, carBoundary, carClassBoundary);

        reservationOriginal.update(reservationNew);
        reservationBoundary.updateReservation(reservationOriginal);
        return Response.ok().build();
    }


    @DELETE
    @Path("{number}")
    public Response deleteReservation(@PathParam("number") long number) {
        boolean deleted = reservationBoundary.deleteReservation(number);
        if (deleted) {
            return Response.ok().build();
        } else {
            throw new NoSuchElementException("reservation not in list");
        }
    }


    private GenericEntity<Collection<ReservationTransport>> getCollectionGenericEntity(Collection<ReservationTransport> carClassCollection) {
        return new GenericEntity<Collection<ReservationTransport>>(carClassCollection) {
        };
    }

}
