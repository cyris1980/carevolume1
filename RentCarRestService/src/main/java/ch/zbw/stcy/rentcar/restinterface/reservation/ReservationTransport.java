package ch.zbw.stcy.rentcar.restinterface.reservation;

import ch.zbw.stcy.rentcar.businesslogic.car.Car;
import ch.zbw.stcy.rentcar.businesslogic.car.ICarBoundary;
import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClass;
import ch.zbw.stcy.rentcar.businesslogic.carClass.ICarClassBoundary;
import ch.zbw.stcy.rentcar.businesslogic.customer.Customer;
import ch.zbw.stcy.rentcar.businesslogic.customer.ICustomerBoundary;
import ch.zbw.stcy.rentcar.businesslogic.reservation.Reservation;

import java.time.LocalDate;

/**
 * Created by stefanallenspach on 29.07.17.
 */
@SuppressWarnings("unused")
public class ReservationTransport {

    private long reservationNo;

    private long customerNo;

    private String customerName;

    private String customerFirstname;

    private String startDate;

    private String endDate;

    private String carClassName;

    private String totalCost;

    private String carIdent;

    public ReservationTransport() {
    }

    public ReservationTransport(Reservation reservation) {
        this.reservationNo = reservation.getReservationNumber();
        this.customerNo = reservation.getCustomer().getNumber();
        this.customerName = reservation.getCustomer().getName();
        this.customerFirstname = reservation.getCustomer().getFirstname();
        this.startDate = reservation.getStartDate().toString();
        this.endDate = reservation.getEndDate().toString();
        this.carClassName = reservation.getCarClass().getName();
        this.totalCost = reservation.getTotalCost().toString();
        this.carIdent = reservation.getCar().map(Car::getIdentification).orElse("");
    }


    public long getReservationNo() {
        return reservationNo;
    }

    public void setReservationNo(long reservationNo) {
        this.reservationNo = reservationNo;
    }

    public long getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(long customerNo) {
        this.customerNo = customerNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerFirstname() {
        return customerFirstname;
    }

    public void setCustomerFirstname(String customerFirstname) {
        this.customerFirstname = customerFirstname;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCarClassName() {
        return carClassName;
    }

    public void setCarClassName(String carClassName) {
        this.carClassName = carClassName;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getCarIdent() {
        return carIdent;
    }

    public void setCarIdent(String carIdent) {
        this.carIdent = carIdent;
    }

    public Reservation getReservation(ICustomerBoundary customerBoundary, ICarBoundary carBoundary, ICarClassBoundary carClassBoundary) throws IllegalArgumentException {

        Customer customer = customerBoundary.getCustomerByNr(this.getCustomerNo())
                .orElseThrow(() -> new IllegalArgumentException("Customer not in list"));

        CarClass carClass = carClassBoundary.getCarClassByName(this.getCarClassName())
                .orElseThrow(() -> new IllegalArgumentException("CarClass not in list"));

        LocalDate startDate = LocalDate.parse(this.startDate);
        LocalDate endDate = LocalDate.parse(this.endDate);

        Reservation reservation = new Reservation(customer, startDate, endDate, carClass);
        reservation.setReservationNumber(reservationNo);
        if (carIdent != null && !carIdent.isEmpty()) {
            Car car = carBoundary.getCarByIdentification(carIdent).orElseThrow(() -> new IllegalArgumentException("car not in list"));
            reservation.setCar(car);
        }

        return reservation;
    }
}
