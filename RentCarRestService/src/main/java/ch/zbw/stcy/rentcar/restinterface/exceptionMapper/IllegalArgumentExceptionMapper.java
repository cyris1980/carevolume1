package ch.zbw.stcy.rentcar.restinterface.exceptionMapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by cyrill on 07.07.17.
 */
@Provider
public class IllegalArgumentExceptionMapper implements ExceptionMapper<IllegalArgumentException> {
    @Override
    public Response toResponse(IllegalArgumentException exception) {
        Configurator.initialize(null, this.getClass().getClassLoader().getResource("Log4J2.xml").toString());
        final Logger logger = LogManager.getLogger(this.getClass());
        logger.error(exception.getMessage());

        return Response.status(Response.Status.BAD_REQUEST)
                .entity(exception.getMessage()).type(MediaType.TEXT_PLAIN).build();
    }
}
