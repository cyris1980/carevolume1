package ch.zbw.stcy.rentcar.businesslogic.reservation;

import ch.zbw.stcy.rentcar.businesslogic.car.Car;
import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClass;
import ch.zbw.stcy.rentcar.businesslogic.customer.Customer;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by stefanallenspach on 12.07.17.
 */
public class ReservationTest {

    private Reservation reservation;
    private Reservation reservation1;
    private Reservation reservation2;

    @Before
    public void setUp() {
        // reservation und reservation1 sind equals
        Customer customer = new Customer("Stefan", "Allenspach");
        CarClass carClass = new CarClass(new BigDecimal("300.00"), "Luxusklasse");
        LocalDate startDate = LocalDate.parse("2017-07-17");
        LocalDate endDate = LocalDate.parse("2017-07-26");
        reservation = new Reservation(customer, startDate, endDate, carClass);

        Customer customer1 = new Customer("Stefan", "Allenspach");
//        CarClass carClass1 = new CarClass(new BigDecimal("300.00"), "Luxusklasse");
        LocalDate startDate1 = LocalDate.parse("2017-07-17");
        LocalDate endDate1 = LocalDate.parse("2017-07-26");
        reservation1 = new Reservation(customer1, startDate1, endDate1, carClass);

        Customer customer2 = new Customer("Stefan", "MisterX");
        CarClass carClass2 = new CarClass(new BigDecimal("600.00"), "Super Luxusklasse");
        LocalDate startDate2 = LocalDate.parse("2017-12-17");
        LocalDate endDate2 = LocalDate.parse("2017-12-26");
        reservation2 = new Reservation(customer2, startDate2, endDate2, carClass2);
    }


    @Test
    public void setAndGetReservatioNumber() {
        reservation.setReservationNumber(12345L);
        assertThat(reservation.getReservationNumber(), is(12345L));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setAndResetReservatioNumber() {
        reservation.setReservationNumber(12345L);
        reservation.setReservationNumber(54321L);
    }

    @Test
    public void setAndGetStartDate() {
        reservation.setStartDate(LocalDate.parse("2017-07-20"));
        assertThat(reservation.getStartDate(), is(LocalDate.parse("2017-07-20")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setAndGetStartDateAfterEndDate() {
        reservation.setEndDate(LocalDate.parse("2017-07-20"));
        reservation.setStartDate(LocalDate.parse("2018-07-20"));
        reservation.isValid();
    }

    @Test(expected = IllegalArgumentException.class)
    public void setAndGetEndDateBeforStartDate() {
        reservation.setStartDate(LocalDate.parse("2017-07-20"));
        reservation.setEndDate(LocalDate.parse("2017-07-19"));
        reservation.isValid();
    }

    @Test
    public void setAndGetEndDate() {
        reservation.setEndDate(LocalDate.parse("2017-07-28"));
        assertThat(reservation.getEndDate(), is(LocalDate.parse("2017-07-28")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setStartDateNull() {
        reservation.setStartDate(null);
        reservation.isValid();
    }

    @Test(expected = IllegalArgumentException.class)
    public void setEndDateNull() {
        reservation.setEndDate(null);
        reservation.isValid();
    }

    @Test
    public void setAndGetCarClass() {
        CarClass carClass = new CarClass(new BigDecimal("200.00"), "Mittelklasse");
        reservation.setCarClass(carClass);
        assertThat(reservation.getCarClass().getName(), is("Mittelklasse"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setCarClassNull() {
        reservation.setCarClass(null);
        reservation.isValid();
    }

    @Test
    public void setAndGetCar() {
        CarClass carClass = new CarClass(new BigDecimal("110.00"), "Einfachklasse");
        Car car = new Car("ABC123", "Skoda", "Combi", carClass);

        assertThat(reservation.getCar().isPresent(), is(false));
        reservation.setCar(car);
        assertThat(reservation.getCar().isPresent(), is(true));
        assertThat(reservation.getCar().get(), is(car));
    }

    @Test
    public void setAndGetCustomer() {
        Customer customer = new Customer("Mister", "X");
        reservation.setCustomer(customer);
        assertThat(reservation.getCustomer(), is(customer));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setCustomerNull() {
        reservation.setCustomer(null);
        reservation.isValid();
    }

    @Test
    public void getTotalCost() {
        assertThat(reservation.getTotalCost(), is(new BigDecimal("3000.00")));
    }

    @Test
    public void isValidationOK() {
        reservation.isValid();
    }

    @Test(expected = IllegalArgumentException.class)
    public void isValidationNotOK() {
        Reservation emptyReservation = new Reservation();
        emptyReservation.isValid();
    }

    @Test
    public void updateReservation() {
        reservation2.setReservationNumber(123L); // ohne DB noch leer

        reservation.setReservationNumber(123L);
        reservation.update(reservation2);

        assertThat(reservation.getCustomer().getName(), is("MisterX"));
        assertThat(reservation.getCarClass().getName(), is("Super Luxusklasse"));
        assertThat(reservation.getStartDate().toString(), is("2017-12-17"));
        assertThat(reservation.getEndDate().toString(), is("2017-12-26"));
        assertThat(reservation.getReservationNumber(), is(123L));
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateReservationWithOtherResNumber() {
        reservation2.setReservationNumber(321L); // ohne DB noch leer

        reservation.setReservationNumber(123L);
        reservation.update(reservation2);

    }

    @Test
    public void isEquals() {
        assertThat(reservation.equals(reservation1), is(true));
    }

    @Test
    public void notEquals() {
        assertThat(reservation.equals(reservation2), is(false));
    }

    @Test
    public void notEqualsReservationNo() {
        reservation2.setReservationNumber(123L);
        assertThat(reservation.equals(reservation2), is(false));
    }

    @Test
    public void notEqualsReservationStartDate() {
        reservation2.setStartDate(LocalDate.parse("1970-01-02"));
        assertThat(reservation.equals(reservation2), is(false));
    }

    @Test
    public void notEqualsReservationEndDate() {
        reservation2.setEndDate(LocalDate.parse("1970-01-02"));
        assertThat(reservation.equals(reservation2), is(false));
    }

    @Test
    public void notEqualsCostumer() {
        reservation2.getCustomer().setName("unbekannt");
        assertThat(reservation.equals(reservation2), is(false));
    }

    @Test
    public void notEqualsCarClass() {
        reservation2.getCarClass().setDayRate(new BigDecimal("99999999"));
        assertThat(reservation.equals(reservation2), is(false));
    }

    @Test
    public void getIdTest() {
        assertThat(reservation.getId(), is(0L));
    }


}