package ch.zbw.stcy.rentcar.businesslogic.car;

import ch.zbw.stcy.rentcar.businesslogic.BoundaryFactory;
import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClass;
import ch.zbw.stcy.rentcar.businesslogic.carClass.ICarClassBoundary;
import org.junit.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by stefanallenspach on 10.07.17.
 */
public class CarBoundaryIT {

    private ICarBoundary boundaryCar;
    private ICarClassBoundary boundaryCarClass;

    private static final String einfachklasse = "Einfachklasse";
    private static final String mittelklasse = "Mittelklasse";
    private CarClass carClassE;
    private CarClass carClassM;
    private Car carNr1;


    @Before
    public void setUp() {
        boundaryCar = BoundaryFactory.createCarBoundary();
        boundaryCarClass = BoundaryFactory.createCarClassBoundary();
        carClassE = boundaryCarClass.getCarClassByName(einfachklasse).get();
        carClassM = boundaryCarClass.getCarClassByName(mittelklasse).get();
        carNr1 = boundaryCar.getCarByIdentification("CR-0001").get();
    }


    @Test
    public void saveAndDeleteCar() {
        final String ident = "FakeCar-1";
        Car car = new Car(ident, "Skoda", "Combi", carClassE);
        Car saveCar = boundaryCar.saveCar(car);

        assertThat(car, is(saveCar));

        //Aufraeumen
        assertThat(boundaryCar.deleteCarByIdentification(ident), is(true));
    }

    @Test (expected = IllegalArgumentException.class)
    public void saveNull(){
        boundaryCar.saveCar(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveCarWithExistingIdentification() {
        final String carIdent = "CR-0001";
        Car car = new Car(carIdent, "FakeBrand", "FakeTyp", carClassE);
        boundaryCar.saveCar(car);
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveCarTwoTimes() {
        final String carIdent = "CR-0001";
        Car savedCar = boundaryCar.getCarByIdentification(carIdent).get();
        boundaryCar.saveCar(savedCar);
    }

    @Test
    public void updateCar() {
        final String carIdent = "CR-0001";
        Car car = new Car(carIdent, "FakeBrand", "FakeTyp", carClassM);
        boundaryCar.updateCar(car);
        Car savedCar = boundaryCar.getCarByIdentification(carIdent).get();
        assertThat(savedCar,is(car));

        //Aufraeumen
        boundaryCar.updateCar(carNr1);
    }

    @Test (expected = IllegalArgumentException.class)
    public void updateCarNotInList() {
        final String carIdent = "987654321";
        Car car = new Car(carIdent, "FakeBrand", "FakeTyp", carClassM);
        boundaryCar.updateCar(car);
    }

    @Test (expected = IllegalArgumentException.class)
    public void updateCarWithNull() {
        boundaryCar.updateCar(null);
    }


    @Test
    public void deleteCarIdentificationNotInList() {
        boolean deleted = boundaryCar.deleteCarByIdentification("fakeCarIdentification");
        assertThat(deleted, is(false));
    }

    @Test
    public void deleteCarIdentificationNull() {
        boolean deleted = boundaryCar.deleteCarByIdentification(null);
        assertThat(deleted, is(false));
    }

    @Test
    public void getAllCars() {
        List<Car> allCarFromCarClass = boundaryCar.getAllCars();
        assertThat(allCarFromCarClass.size(), is(18));
    }

    @Test
    public void getAllCarsFromCarClass() {
        List<Car> allCarFromCarClass = boundaryCar.getAllCarsByCarClass(carClassE);
        assertThat(allCarFromCarClass.size(), is(not(0)));

        for (Car car : allCarFromCarClass) {
            assertThat(car.getCarClass(), is(carClassE));
        }
    }

    @Test
    public void getCarByIdentification() {
        final String carIdent = "CR-0001";
        Optional<Car> opCar = boundaryCar.getCarByIdentification(carIdent);

        assertThat(opCar.isPresent(), is(true));
        assertThat(opCar.get().getIdentification(), is(carIdent));
    }

    @Test
    public void getCarByIdentificationNotInList() {
        final String carIdent = "Fake-Idnet";
        Optional<Car> opCar = boundaryCar.getCarByIdentification(carIdent);

        assertThat(opCar.isPresent(), is(false));
    }

    @Test
    public void getCarByIdentificationNull() {
        Optional<Car> opCar = boundaryCar.getCarByIdentification(null);
        assertThat(opCar.isPresent(), is(false));
    }

    @Test
    public void editCarTyp() {
        final String carIdent = "CR-0002";
        final String newTyp = "Kombi XL";
        final String originalTyp = "Kombi";

        Optional<Car> opCar = boundaryCar.getCarByIdentification(carIdent);
        Car car = opCar.get();
        car.setTyp(newTyp);
        Car savedCar = boundaryCar.updateCar(car);

        assertThat(savedCar.getTyp(), is(newTyp));

        //Aufraeumen inkl. kontrolle
        car.setTyp(originalTyp);
        boundaryCar.updateCar(car);
        assertThat(boundaryCar.getCarByIdentification(carIdent).get().getTyp(), is(originalTyp));
    }

    @Test
    public void editCarSetOtherCarClass() {
        final String carIdent = "CR-0001";
        final CarClass otherCarClass = carClassM;
        final CarClass originalCarClass = carClassE;

        Optional<Car> opCar = boundaryCar.getCarByIdentification(carIdent);
        Car car = opCar.get();
        car.setCarClass(otherCarClass);
        Car savedCar = boundaryCar.updateCar(car);

        assertThat(savedCar.getCarClass(), is(otherCarClass));

        //Aufraeumen inkl. kontrolle
        car.setCarClass(originalCarClass);
        boundaryCar.updateCar(car);
        assertThat(boundaryCar.getCarByIdentification(carIdent).get().getCarClass(), is(originalCarClass));
    }

}
