package ch.zbw.stcy.rentcar.businesslogic.customer;

import ch.zbw.stcy.rentcar.infrastructure.RentCarValidationException;
import ch.zbw.stcy.rentcar.infrastructure.commons.Utils;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class AddressTest {

    @Test
    public void isValidTest() {
        Address[] testAddresses = getTestAddresses();
        assertTrue(Utils.validate(testAddresses[0]));
        assertTrue(Utils.validate(testAddresses[1]));
    }

    @Test(expected = RentCarValidationException.class)
    public void isValidTest_cityNull() {
        Address[] testAddresses = getTestAddresses();
        testAddresses[0].setCity(null);
        Utils.validate(testAddresses[0]);
    }

    @Test(expected = RentCarValidationException.class)
    public void isValidTest_countryNull() {
        Address[] testAddresses = getTestAddresses();
        testAddresses[0].setCountry(null);
        Utils.validate(testAddresses[0]);
    }

    @Test(expected = RentCarValidationException.class)
    public void isValidTest_streetNull() {
        Address[] testAddresses = getTestAddresses();
        testAddresses[0].setStreet(null);
        Utils.validate(testAddresses[0]);
    }

    @Test(expected = RentCarValidationException.class)
    public void isValidTest_zipCodeNull() {
        Address[] testAddresses = getTestAddresses();
        testAddresses[0].setPostZip(null);
        Utils.validate(testAddresses[0]);
    }

    @Test(expected = RentCarValidationException.class)
    public void validateToShortZipCode() {
        Address[] testAddresses = getTestAddresses();
        testAddresses[0].setPostZip("333");
        Utils.validate(testAddresses[0]);

    }

    @Test
    public void testGettersAndSetters() {
        Address[] testAddresses = getTestAddresses();
        Address testAddress = testAddresses[0];
        assertThat(testAddress.getCity() ,is("Goldach"));
        assertThat(testAddress.getCountry() ,is("CH"));
        assertThat(testAddress.getPostZip() ,is("8503"));
        assertThat(testAddress.getStreet() ,is("Thannstrasse 7"));
    }

    private Address[] getTestAddresses(){
        Address address1 = new Address();
        address1.setCity("Goldach");
        address1.setCountry("CH");
        address1.setPostZip("8503");
        address1.setStreet("Thannstrasse 7");

        Address address2 = new Address();
        address2.setCity("München");
        address2.setCountry("DE");
        address2.setPostZip("90000");
        address2.setStreet("Bierstrasse");
        return new Address[]{address1, address2};
    }

}