package ch.zbw.stcy.rentcar.businesslogic.reservation;

import ch.zbw.stcy.rentcar.businesslogic.BoundaryFactory;
import ch.zbw.stcy.rentcar.businesslogic.car.Car;
import ch.zbw.stcy.rentcar.businesslogic.car.ICarBoundary;
import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClass;
import ch.zbw.stcy.rentcar.businesslogic.carClass.ICarClassBoundary;
import ch.zbw.stcy.rentcar.businesslogic.customer.Customer;
import ch.zbw.stcy.rentcar.businesslogic.customer.ICustomerBoundary;
import ch.zbw.stcy.rentcar.infrastructure.RepositoryException;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

/**
 * Created by stefanallenspach on 17.07.17.
 */
public class ReservationBoundaryIT {
    private IReservationBoundary boundaryReservation;
    private ICustomerBoundary boundaryCustomer;
    private ICarClassBoundary boundaryCarClass;
    private ICarBoundary boundaryCar;

    private Reservation reservation;
    private Car car;
    private CarClass carClass;
    private Customer customer;


    @Before
    public void setUp() {
        boundaryReservation = BoundaryFactory.createReservationBoundary();
        boundaryCustomer = BoundaryFactory.createCustomerBoundary();
        boundaryCarClass = BoundaryFactory.createCarClassBoundary();
        boundaryCar = BoundaryFactory.createCarBoundary();

        car = boundaryCar.getCarByIdentification("CR-0001").orElseThrow(AssertionError::new);
        carClass = boundaryCarClass.getCarClassByName("Einfachklasse").orElseThrow(AssertionError::new);
        customer = boundaryCustomer.getCustomerByNr(1L).orElseThrow(AssertionError::new);

        LocalDate startDate = LocalDate.parse("1977-12-29");
        LocalDate endDate = LocalDate.parse("1977-12-29");
        reservation = new Reservation(customer, startDate, endDate, carClass);

    }


    @Test
    public void getReservationByNumber() {
        Optional<Reservation> opReservation = boundaryReservation.getReservationByNr(1L);
        boolean present = opReservation.isPresent();
        assertThat(present, is(true));
        if(present) {
            Reservation savedReservation = opReservation.get();
            assertThat(savedReservation.getReservationNumber(), is(1L));
        }
    }

    @Test
    public void getReservationByNumberNotInList() {
        Optional<Reservation> opReservation = boundaryReservation.getReservationByNr(987654321L);
        assertThat(opReservation.isPresent(), is(false));
    }

    @Test
    public void setCarToReservation() {
        Optional<Reservation> opReservation = boundaryReservation.getReservationByNr(1L);
        boolean present = opReservation.isPresent();
        assertThat(present, is(true));
        if(present) {
            Reservation reservation = opReservation.get();
            reservation.setCar(car);
            Reservation saveReservation = boundaryReservation.updateReservation(reservation);

            assertThat(saveReservation.getCar().get(), is(car));

            //Aufraeumen inkl. Kontrolle
            saveReservation.setCar(null);
            saveReservation = boundaryReservation.updateReservation(saveReservation);
            assertThat(saveReservation.getCar().isPresent(), is(false));
        }
    }

    @Test
    public void saveAndDeleteReservation() {
        Reservation saveReservation = boundaryReservation.saveReservation(reservation);
        long resNo = saveReservation.getReservationNumber();
        reservation.setReservationNumber(resNo);
        assertThat(saveReservation, is(reservation));

        //Aufraeumen inkl. Kontrolle
        boolean deleted = boundaryReservation.deleteReservation(saveReservation.getReservationNumber());
        assertThat(deleted, is(true));
        Optional<Reservation> opReservation = boundaryReservation.getReservationByNr(saveReservation.getReservationNumber());
        assertThat(opReservation.isPresent(), is(false));
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveNull() {
        boundaryReservation.saveReservation(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNull() {
        boundaryReservation.updateReservation(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveNotValidReservation() {
        boundaryReservation.saveReservation(new Reservation());
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveExistingReservationsNumber() {
        reservation.setReservationNumber(1L);
        boundaryReservation.saveReservation(reservation);
    }

    @Test
    public void deleteReservationThatNotExists() {
        boolean deleted = boundaryReservation.deleteReservation(987654321L);
        assertThat(deleted, is(false));
    }

    @Test
    public void getReservationByStartdate() {
        final LocalDate startDate = LocalDate.parse("2017-01-01");
        List<Reservation> reservationsByStartDate = boundaryReservation.getReservationByStartDate(startDate);

        assertThat(reservationsByStartDate.size(), is(not(0)));

        for (Reservation reservation : reservationsByStartDate) {
            assertThat(reservation.getStartDate(), is(startDate));
        }
    }

    @Test
    public void getReservationByStartdateNotExists() {
        final LocalDate startDate = LocalDate.parse("3000-01-01");
        List<Reservation> reservationsByStartDate = boundaryReservation.getReservationByStartDate(startDate);

        assertThat(reservationsByStartDate.size(), is(0));
    }


    @Test
    public void getAllReservation() {
        List<Reservation> allReservation = boundaryReservation.getAllReservation();
        assertThat(allReservation.size(), is(9));
    }

    @Test
    public void getAllReservationByCustomer() {
        List<Reservation> allReservation = boundaryReservation.getAllReservationFromCustomer(customer);
        assertThat(allReservation.size(), is(1));
    }

    @Test
    public void getAllReservationByCustomerNull() {
        List<Reservation> allReservation = boundaryReservation.getAllReservationFromCustomer(null);
        assertThat(allReservation.size(), is(0));
    }

    @Test
    public void getAllReservationByCustomerNotReservation() {
        Customer customerNoRes = boundaryCustomer.getCustomerByNr(30L).orElseThrow(AssertionError::new);
        List<Reservation> allReservation = boundaryReservation.getAllReservationFromCustomer(customerNoRes);
        assertThat(allReservation.size(), is(0));
    }

    @Test(expected = RepositoryException.class)
    public void getReservationByDateNull() {
        boundaryReservation.getReservationByStartDate(null);
    }

}
