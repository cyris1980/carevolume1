package ch.zbw.stcy.rentcar.businesslogic.car;

import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClass;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * Created by stefanallenspach on 12.07.17.
 */
public class CarTest {

    private Car car;
    private CarClass carClass;

    @Before
    public void setUp() {
        carClass = new CarClass(new BigDecimal("110.00"), "Einfachklasse");
        car = new Car("ABC123", "Skoda", "Combi", carClass);
    }


    @Test(expected = IllegalArgumentException.class)
    public void setNewIdentification() {
        car.setIdentification("xyz");
        assertThat(car.getIdentification(), is("xyz"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setIdentificationNull() {
        car.setIdentification(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setIdentificationEmpty() {
        car.setIdentification("");
    }

    @Test
    public void setAndGetBrand() {
        car.setBrand("Honda");
        assertThat(car.getBrand(), is("Honda"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setBrandNull() {
        car.setBrand(null);
        car.isValid();
    }

    @Test(expected = IllegalArgumentException.class)
    public void setAndGetBrandEmpty() {
        car.setBrand("");
        car.isValid();
    }

    @Test
    public void setAndGetTyp() {
        car.setTyp("Sportwagen");
        assertThat(car.getTyp(), is("Sportwagen"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setTypNull() {
        car.setTyp(null);
        car.isValid();
    }

    @Test(expected = IllegalArgumentException.class)
    public void setTypEmpty() {
        car.setTyp("");
        car.isValid();
    }

    @Test
    public void setAndGetCarClass() {
        CarClass newCarClass = new CarClass(new BigDecimal("200.00"), "Mittelklasse");
        car.setCarClass(newCarClass);
        assertThat(car.getCarClass().getName(), is("Mittelklasse"));
        assertThat(car.getCarClass(), is(newCarClass));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setAndGetCarClassNull() {
        car.setCarClass(null);
        car.isValid();
    }

    @Test
    public void testEquals() {
        CarClass carClass1 = new CarClass(new BigDecimal("110.00"), "Einfachklasse");
        CarClass carClass2 = new CarClass(new BigDecimal("110.00"), "Einfachklasse");
        Car car1 = new Car("ABC123", "Skoda", "Combi", carClass1);
        Car car2 = new Car("ABC123", "Skoda", "Combi", carClass2);
        assertEquals(car1, car2);
    }

    @Test
    public void testNotEqualsIdent() {
        Car car1 = new Car("ABC", "Skoda", "Combi", carClass);
        Car car2 = new Car("ABC123", "Skoda", "Combi", carClass);
        assertNotEquals(car1, car2);
    }

    @Test
    public void testNotEqualsBrand() {
        Car car1 = new Car("ABC123", "Honda", "Combi", carClass);
        Car car2 = new Car("ABC123", "Skoda", "Combi", carClass);
        assertNotEquals(car1, car2);
    }

    @Test
    public void testNotEqualsTyp() {
        Car car1 = new Car("ABC123", "Skoda", "Limo", carClass);
        Car car2 = new Car("ABC123", "Skoda", "Combi", carClass);
        assertNotEquals(car1, car2);
    }

    @Test
    public void testNotEqualsCarClass() {
        CarClass carClass1 = new CarClass(new BigDecimal("110.00"), "Einfachklasse");
        CarClass carClass2 = new CarClass(new BigDecimal("110.00"), "Mittelklasse");
        Car car1 = new Car("ABC123", "Skoda", "Combi", carClass1);
        Car car2 = new Car("ABC123", "Skoda", "Combi", carClass2);
        assertNotEquals(car1, car2);
    }

    @Test
    public void testToString() {
        assertThat(car.toString(), containsString("ABC123"));
        assertThat(car.toString(), containsString("Skoda"));
        assertThat(car.toString(), containsString("Combi"));
    }

    @Test
    public void testValidationOK() {
        assertThat(car.isValid(), is(true));
    }

    @Test
    public void testUpdateEqualsCar() {
        Car car1 = new Car("ABC123", "Skoda", "Combi", carClass);
        Car car2 = new Car("ABC123", "Skoda", "Combi", carClass);
        car1.update(car2);
        assertEquals(car1, car2);
    }

    @Test
    public void testUpdateBrand() {
        Car car1 = new Car("ABC123", "Skoda", "Combi", carClass);
        Car car2 = new Car("ABC123", "Honda", "Combi", carClass);
        car1.update(car2);
        assertThat(car1.getBrand(), is("Honda"));
    }

    @Test
    public void testUpdateTyp() {
        Car car1 = new Car("ABC123", "Skoda", "Combi", carClass);
        Car car2 = new Car("ABC123", "Skoda", "Kleinwagen", carClass);
        car1.update(car2);
        assertThat(car1.getTyp(), is("Kleinwagen"));
    }

    @Test
    public void testUpdateCarClass() {
        CarClass carClass2 = new CarClass(new BigDecimal("110.00"), "Mittelklasse");

        Car car1 = new Car("ABC123", "Skoda", "Combi", carClass);
        Car car2 = new Car("ABC123", "Skoda", "Kleinwagen", carClass2);
        car1.update(car2);
        assertThat(car1.getCarClass(), is(carClass2));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testUpdateWithNull() {
        car.update(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateIdentification() {
        Car car1 = new Car("ABC123", "Skoda", "Combi", carClass);
        Car car2 = new Car("XYZ123", "Skoda", "Combi", carClass);
        car1.update(car2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidationIdentificationNull() {
        Car newCar = new Car(null, "Skoda", "Combi", carClass);
        newCar.isValid();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidationIdentificationEmpty() {
        Car newCar = new Car("", "Skoda", "Combi", carClass);
        newCar.isValid();
    }


    @Test
    public void testGetId() {
        assertThat(car.getId(), is(0L));
    }

}