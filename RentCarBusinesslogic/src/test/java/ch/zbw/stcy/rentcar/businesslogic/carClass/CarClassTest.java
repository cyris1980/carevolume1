package ch.zbw.stcy.rentcar.businesslogic.carClass;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by stefanallenspach on 12.07.17.
 */
public class CarClassTest {

    private CarClass carClass;

    @Before
    public void setUp() {
        carClass = new CarClass(new BigDecimal("300.00"), "Luxusklasse");
    }

    @Test
    public void testIsValide() {
        assertThat(carClass.isValid(), is(true));
    }

    @Test
    public void testSetGetDayRate() {
        carClass.setDayRate(new BigDecimal("400"));
        assertThat(carClass.getDayRate(), is(new BigDecimal("400")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetDayRateNull() {
        carClass.setDayRate(null);
        carClass.isValid();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetDayRateNullLess0() {
        carClass.setDayRate(new BigDecimal("-1"));
        carClass.isValid();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOverrideExistingName() {
        carClass.setName("Mittelklasse");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetNameNull() {
        carClass.setName(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetNoName() {
        CarClass carClass1 = new CarClass();
        carClass1.setDayRate(new BigDecimal("100.00"));
        carClass1.isValid();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyNameInConstructor() {
        CarClass carClass1 = new CarClass(new BigDecimal("100"), "");
        carClass1.isValid();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetNameEmpty() {
        carClass.setName("");
    }

    @Test
    public void testEquals() {
        CarClass carClass1 = new CarClass(new BigDecimal("300.00"), "Luxusklasse");
        CarClass carClass2 = new CarClass(new BigDecimal("300.00"), "Luxusklasse");

        assertEquals(carClass1, carClass2);
    }

    @Test
    public void testNotEqualsDayRate() {
        CarClass carClass1 = new CarClass(new BigDecimal("300"), "Luxusklasse");
        CarClass carClass2 = new CarClass(new BigDecimal("300.00"), "Luxusklasse");

        assertNotEquals(carClass1, carClass2);
    }

    @Test
    public void testNotEqualsName() {
        CarClass carClass1 = new CarClass(new BigDecimal("300.00"), "Mittelklasse");
        CarClass carClass2 = new CarClass(new BigDecimal("300.00"), "Luxusklasse");

        assertNotEquals(carClass1, carClass2);
    }

    @Test
    public void testToString() {
        CarClass carClass1 = new CarClass(new BigDecimal("300.00"), "Luxusklasse");

        assertThat(carClass1.toString(), containsString("300.00"));
        assertThat(carClass1.toString(), containsString("Luxusklasse"));
    }

    @Test
    public void testUpdate() {
        CarClass carClass1 = new CarClass(new BigDecimal("300.00"), "Luxusklasse");
        CarClass carClass2 = new CarClass(new BigDecimal("200.00"), "Luxusklasse");
        carClass1.update(carClass2);

        assertThat(carClass1.getDayRate(), is(new BigDecimal("200.00")));
    }

    @Test
    public void testUpdateEqualsCarClass() {
        CarClass carClass1 = new CarClass(new BigDecimal("300.00"), "Luxusklasse");
        CarClass carClass2 = new CarClass(new BigDecimal("300.00"), "Luxusklasse");
        carClass1.update(carClass2); // Es darf keine Exception geworfen werden
        assertThat(carClass1, is(carClass2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateName() {
        CarClass carClass1 = new CarClass(new BigDecimal("300.00"), "Luxusklasse");
        CarClass carClass2 = new CarClass(new BigDecimal("300.00"), "Super Luxusklasse");
        carClass1.update(carClass2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateNameWithNull() {
        CarClass carClass1 = new CarClass(new BigDecimal("300.00"), "Luxusklasse");
        CarClass carClass2 = new CarClass(new BigDecimal("300.00"), null);
        carClass1.update(carClass2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateWithNull() {
        carClass.update(null);
    }

    @Test
    public void testGetId() {
        assertThat(carClass.getId(), is(0L));
    }
}