package ch.zbw.stcy.rentcar.businesslogic.carClass;

import ch.zbw.stcy.rentcar.businesslogic.BoundaryFactory;
import ch.zbw.stcy.rentcar.infrastructure.RepositoryException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by stefanallenspach on 09.07.17.
 */
public class CarClassBoundaryIT {

    private ICarClassBoundary boundary;
    private static final String einfachklasse = "Einfachklasse";


    @Before
    public void setUp() {
        boundary = BoundaryFactory.createCarClassBoundary();

    }

    @Test
    public void saveAndDeleteCarClass() {
        final String carClassName = "Transporter";
        CarClass carClass = new CarClass(new BigDecimal("250"), carClassName);

        CarClass savedCarClass = boundary.saveCarClass(carClass);
        assertThat(carClass, is(savedCarClass));

        //delete
        assertThat(boundary.deleteCarClass(savedCarClass), is(true));
    }
    @Test
    public void saveAndDeleteCarClassByName() {
        final String carClassName = "Motorrad";
        CarClass carClass = new CarClass(new BigDecimal("220"), carClassName);

        CarClass savedCarClass = boundary.saveCarClass(carClass);
        assertThat(carClass, is(savedCarClass));

        //delete
        assertThat(boundary.deleteCarClassByName(carClassName), is(true));
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveCarClassWithExistingName() {
        CarClass carClass = new CarClass(new BigDecimal("999"), einfachklasse);
        boundary.saveCarClass(carClass);
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveNull() {
        boundary.saveCarClass(null);
    }

    @Test
    public void deleteCarClassByNameNotInList(){
        boolean deleted = boundary.deleteCarClassByName("FakeCarClass");
        assertThat(deleted, is(false));
    }

    @Test
    public void deleteNameNull(){
        boolean deleted = boundary.deleteCarClassByName(null);
        assertThat(deleted, is(false));
    }

    @Test (expected = IllegalArgumentException.class)
    public void deleteNull(){
        boundary.deleteCarClass(null);
    }

    @Test
    public void getCarClassByName() {
        Optional<CarClass> opCarClass = boundary.getCarClassByName(einfachklasse);
        assertThat(opCarClass.isPresent(), is(true));

        CarClass carClass = opCarClass.get();
        assertThat(carClass.getName(), is(einfachklasse));
    }

    @Test
    public void getCarClassByNameNotInList() {
        Optional<CarClass> opCarClass = boundary.getCarClassByName("NotInList");
        assertThat(opCarClass.isPresent(), is(false));
    }

    @Test
    public void getAllCarClasses() {
        List<CarClass> allCarClasses = boundary.getAllCarClasses();
        assertThat(allCarClasses.size(), is(3));
    }


    @Test
    public void updateCarClass() {
        CarClass carClass = boundary.getCarClassByName(einfachklasse).get();
        carClass.setDayRate(new BigDecimal("150"));
        CarClass saveCarClass = boundary.updateCarClass(carClass);

        assertThat(saveCarClass.getDayRate(), is(new BigDecimal("150")));

        //Aufraeumen inkl. kontrolle
        carClass.setDayRate(new BigDecimal("100"));
        carClass = boundary.updateCarClass(carClass);
        assertThat(carClass.getDayRate(), is(new BigDecimal("100")));
    }

    @Test (expected = IllegalArgumentException.class)
    public void updateCarClassWithNull() {
        boundary.updateCarClass(null);
    }

    @Test (expected = NoSuchElementException.class)
    public void updateCarClassNotInList() {
        CarClass carClassNotInList = new CarClass(new BigDecimal("100"), "NotInList");
        boundary.updateCarClass(carClassNotInList);
    }

}
