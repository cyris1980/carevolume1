package ch.zbw.stcy.rentcar.businesslogic.customer;

import ch.zbw.stcy.rentcar.infrastructure.RentCarValidationException;
import ch.zbw.stcy.rentcar.infrastructure.commons.Utils;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class CustomerTest {


    @Test
    public void customerUpdateTest() {
        Customer[] testCustomers = getTestCustomers();
        assertThat(testCustomers[0].getName(), is("Muster"));
        assertThat(testCustomers[0].getFirstname(), is("Hans"));
        assertThat(testCustomers[0].getAddress().getCity(), is("Goldach"));
        assertThat(testCustomers[0].getAddress().getPostZip(), is("8503"));
        assertThat(testCustomers[0].getAddress().getCountry(), is("CH"));
        assertThat(testCustomers[0].getAddress().getStreet(), is("Thannstrasse 7"));
        boolean validate = Utils.validate(testCustomers[0]);
        assertTrue(validate);
        validate = Utils.validate(testCustomers[1]);
        assertTrue(validate);

        Utils.validate(testCustomers[0]);
        Utils.validate(testCustomers[1]);


        // Felder des testCustomers[0] mit denen des testCustomer[1] ueberschreiben

        testCustomers[0].update(testCustomers[1]);
        // testen ob alle Felder den erwarteten Inhalt haben

        assertThat(testCustomers[0].getName(), is("Kohl"));
        assertThat(testCustomers[0].getFirstname(), is("Helmut"));
        assertThat(testCustomers[0].getAddress().getCity(), is("München"));
        assertThat(testCustomers[0].getAddress().getPostZip(), is("90000"));
        assertThat(testCustomers[0].getAddress().getCountry(), is("DE"));
        assertThat(testCustomers[0].getAddress().getStreet(), is("Bierstrasse"));

        validate = Utils.validate(testCustomers[0]);
        assertTrue(validate);
    }

    @Test(expected = RentCarValidationException.class)
    public void customUpdateTest_NullAddress() {
        Customer[] testCustomers = getTestCustomers();
        testCustomers[0].setAddress(null);
        Utils.validate(testCustomers[0]);
    }

    @Test(expected = RentCarValidationException.class)
    public void customUpdateTest_NameNull() {
        Customer[] testCustomers = getTestCustomers();
        testCustomers[0].setName(null);
        Utils.validate(testCustomers[0]);
    }

    @Test(expected = RentCarValidationException.class)
    public void customUpdateTest_FirstNameNull() {
        Customer[] testCustomers = getTestCustomers();
        testCustomers[0].setFirstname(null);
        Utils.validate(testCustomers[0]);

    }

    private Customer[] getTestCustomers() {
        Customer customer1 = new Customer();
        customer1.setName("Muster");
        customer1.setFirstname("Hans");

        Address address = new Address();
        address.setCity("Goldach");
        address.setCountry("CH");
        address.setPostZip("8503");
        address.setStreet("Thannstrasse 7");
        customer1.setAddress(address);


        Customer customer2 = new Customer();
        customer2.setName("Kohl");
        customer2.setFirstname("Helmut");
        address = new Address();
        address.setCity("München");
        address.setCountry("DE");
        address.setPostZip("90000");
        address.setStreet("Bierstrasse");
        customer2.setAddress(address);

        return new Customer[]{customer1, customer2};

    }


}
