package ch.zbw.stcy.rentcar.businesslogic.customer;


import ch.zbw.stcy.rentcar.businesslogic.BoundaryFactory;
import ch.zbw.stcy.rentcar.infrastructure.RentCarValidationException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by cyrill on 26.05.17.
 */
public class CustomerBoundaryIT {

    private ICustomerBoundary boundary;

    @Before
    public void setUp() {
        boundary = BoundaryFactory.createCustomerBoundary();
    }

    @Test
    public void saveCustomer() {
        Customer cust1 = new Customer("Allenspach", "Stefan");
        Address address1 = new Address("Wildeggstrasse 32", "9000", "Sankt Gallen", "CH");
        cust1.setAddress(address1);

        Customer cust2 = new Customer("Schwyter", "Cyrill");
        Address address2 = new Address("Hellmühlestr. 24a", "8580", "Amriswil", "CH");
        cust2.setAddress(address2);

        // Customer 1 und 2 hinzufuegen
        cust1 = boundary.saveCustomer(cust1);
        cust2 = boundary.saveCustomer(cust2);
        final long custNr1 = cust1.getNumber();
        final long custNr2 = cust2.getNumber();


        // Customer 1 und 2 aus DB lesen
        Customer saved1 = boundary.getCustomerByNr(custNr1).get();
        Customer saved2 = boundary.getCustomerByNr(custNr2).get();
        assertEquals(saved1, cust1);
        assertEquals(saved2, cust2);

        // Customer 1 und 2 aud der DB loeschen
        assertTrue(boundary.deleteCustomer(custNr1));
        assertTrue(boundary.deleteCustomer(custNr2));

        // Customer 1 und 2 duerfen noch mehr in der List sein.
        assertFalse(boundary.getCustomerByNr(custNr1).isPresent());
        assertFalse(boundary.getCustomerByNr(custNr2).isPresent());


    }

    @Test(expected = IllegalArgumentException.class)
    public void saveCustomerNullTest() {
        boundary.saveCustomer(null);
    }

    @Test
    public void editCustomer() {

        Customer customer = boundary.getCustomerByNr(1).get();

        final String oldFirstname = customer.getFirstname();
        final String oldName = customer.getName();
        final String oldStreet = customer.getAddress().getStreet();
        final String oldPostZip = customer.getAddress().getPostZip();
        final String oldCity = customer.getAddress().getCity();
        final String oldCountry = customer.getAddress().getCountry();

        final String newFirstname = "John";
        final String newName = "Doe";
        final String newStreet = "Endstreet";
        final String newPostZip = "6666";
        final String newCity = "SinCity";
        final String newCountry = "Hell";

        //Customer editieren und anschliessend speichern
        customer.setName(newName);
        customer.setFirstname(newFirstname);
        customer.getAddress().setStreet(newStreet);
        customer.getAddress().setPostZip(newPostZip);
        customer.getAddress().setCity(newCity);
        customer.getAddress().setCountry(newCountry);
        boundary.updateCustomer(customer);


        //Editierter Customer lesen und ueberpruefen
        Customer edit1 = boundary.getCustomerByNr(1).get();
        assertEquals(newFirstname, edit1.getFirstname());
        assertEquals(newName, edit1.getName());
        assertEquals(newStreet, edit1.getAddress().getStreet());
        assertEquals(newPostZip, edit1.getAddress().getPostZip());
        assertEquals(newCity, edit1.getAddress().getCity());
        assertEquals(newCountry, edit1.getAddress().getCountry());


        // In Customer in den Ursprungszustand zurueck versetzen
        Customer old = boundary.getCustomerByNr(1).get();
        old.setName(oldName);
        old.setFirstname(oldFirstname);
        old.getAddress().setStreet(oldStreet);
        old.getAddress().setPostZip(oldPostZip);
        old.getAddress().setCity(oldCity);
        old.getAddress().setCountry(oldCountry);
        boundary.updateCustomer(old);

    }


    @Test
    public void getAllCustomers() {
        List<Customer> resultList = boundary.getAllCustomers();
        assertEquals(30, resultList.size());
    }

    @Test
    public void searchCustomers() {

        //Suchen nach einer Nummer die nicht existiert
        List<Customer> resultList0 = boundary.searchCustomers("888888");
        assertEquals(0, resultList0.size());


        //Suche nach einem Namen der nicht in der Liste ist
        List<Customer> resultList1 = boundary.searchCustomers("100001 MisterX_does_not_exists 100001");
        assertEquals(0, resultList1.size());


        //Suche nach einem Namen der einmal in der Liste ist
        List<Customer> resultList2 = boundary.searchCustomers("Allenspach");
        assertEquals(1, resultList2.size());

        Customer customer1 = resultList2.get(0);
        assertEquals("Stefan", customer1.getFirstname());
        assertEquals("Allenspach", customer1.getName());
        assertEquals("Wildeggstrasse 32", customer1.getAddress().getStreet());
        assertEquals("9000", customer1.getAddress().getPostZip());
        assertEquals("St. Gallen", customer1.getAddress().getCity());
        assertEquals("CH", customer1.getAddress().getCountry());


        //Suche nach einem Namen der zweimal in der Liste ist
        List<Customer> resultList3 = boundary.getCustomerByName("Schwyter");
        assertEquals(2, resultList3.size());

        Customer customer2_1 = resultList3.get(0);
        assertEquals("Cyrill", customer2_1.getFirstname());
        assertEquals("Schwyter", customer2_1.getName());
        assertEquals("Hellmühlestr. 24a", customer2_1.getAddress().getStreet());
        assertEquals("8580", customer2_1.getAddress().getPostZip());
        assertEquals("Amriswil", customer2_1.getAddress().getCity());
        assertEquals("CH", customer2_1.getAddress().getCountry());

        Customer customer2_2 = resultList3.get(1);
        assertEquals("Florian", customer2_2.getFirstname());
        assertEquals("Schwyter", customer2_2.getName());
        assertEquals("Bruggerstrasse 25", customer2_2.getAddress().getStreet());
        assertEquals("7320", customer2_2.getAddress().getPostZip());
        assertEquals("Sargans", customer2_2.getAddress().getCity());
        assertEquals("CH", customer2_2.getAddress().getCountry());


        //Suche nach einem Nummer der einmal in der Liste ist
        List<Customer> resultList4 = boundary.searchCustomers("1");
        assertEquals(1, resultList4.size());

        Customer customer4 = resultList2.get(0);
        assertEquals("Stefan", customer4.getFirstname());
        assertEquals("Allenspach", customer4.getName());
        assertEquals("Wildeggstrasse 32", customer4.getAddress().getStreet());
        assertEquals("9000", customer4.getAddress().getPostZip());
        assertEquals("St. Gallen", customer4.getAddress().getCity());
        assertEquals("CH", customer4.getAddress().getCountry());

        //Suche nach einem null String
        List<Customer> resultList5 = boundary.searchCustomers(null);
        assertEquals(0, resultList5.size());


        //Suche nach einem leer String
        List<Customer> resultList6 = boundary.searchCustomers("");
        assertEquals(30, resultList6.size());


        //Suche nach einem Vornamen der einmal existiert
        List<Customer> resultList7 = boundary.searchCustomers("Cyrill");
        assertEquals(1, resultList7.size());


        //Suche nach einem Vornamen der mehrmals existiert
        List<Customer> resultList8 = boundary.searchCustomers("Daniel");
        assertEquals(5, resultList8.size());

        //Suche nach einem Teil-String
        List<Customer> resultList9 = boundary.searchCustomers("e");
        resultList9.forEach(System.out ::println);
        assertEquals(28, resultList9.size());

    }

    @Test
    public void getCustomerByNr() {

        //Gib eine Nummer die nicht existiert
        assertFalse(boundary.getCustomerByNr(888888).isPresent());

        //Gib eine Nummer die existiert
        Customer customer = boundary.getCustomerByNr(2).get();
        assertEquals("Cyrill", customer.getFirstname());
        assertEquals("Schwyter", customer.getName());
        assertEquals("Hellmühlestr. 24a", customer.getAddress().getStreet());
        assertEquals("8580", customer.getAddress().getPostZip());
        assertEquals("Amriswil", customer.getAddress().getCity());
        assertEquals("CH", customer.getAddress().getCountry());


    }

    @Test
    public void getCustomerByName() {

        //Suchen nach einem NULL Wert
        List<Customer> resultList = boundary.getCustomerByName(null);
        assertEquals(0, resultList.size());


        //Suche nach einem Namen der nicht in der Liste ist
        List<Customer> resultList0 = boundary.getCustomerByName("MisterX_does_not_exists");
        assertEquals(0, resultList0.size());


        //Suche nach einem Namen der einmal in der Liste ist
        List<Customer> resultList1 = boundary.getCustomerByName("Allenspach");
        assertEquals(1, resultList1.size());

        Customer customer1 = resultList1.get(0);
        assertEquals("Stefan", customer1.getFirstname());
        assertEquals("Allenspach", customer1.getName());
        assertEquals("Wildeggstrasse 32", customer1.getAddress().getStreet());
        assertEquals("9000", customer1.getAddress().getPostZip());
        assertEquals("St. Gallen", customer1.getAddress().getCity());
        assertEquals("CH", customer1.getAddress().getCountry());


        //Suche nach einem Namen der zweimal in der Liste ist
        List<Customer> resultList2 = boundary.getCustomerByName("Schwyter");
        assertEquals(2, resultList2.size());

        Customer customer2_1 = resultList2.get(0);
        assertEquals("Cyrill", customer2_1.getFirstname());
        assertEquals("Schwyter", customer2_1.getName());
        assertEquals("Hellmühlestr. 24a", customer2_1.getAddress().getStreet());
        assertEquals("8580", customer2_1.getAddress().getPostZip());
        assertEquals("Amriswil", customer2_1.getAddress().getCity());
        assertEquals("CH", customer2_1.getAddress().getCountry());

        Customer customer2_2 = resultList2.get(1);
        assertEquals("Florian", customer2_2.getFirstname());
        assertEquals("Schwyter", customer2_2.getName());
        assertEquals("Bruggerstrasse 25", customer2_2.getAddress().getStreet());
        assertEquals("7320", customer2_2.getAddress().getPostZip());
        assertEquals("Sargans", customer2_2.getAddress().getCity());
        assertEquals("CH", customer2_2.getAddress().getCountry());


    }

    @Test
    public void deleteCustomer() {

        //Hinzu fuegen eines Customers
        Customer customer = new Customer("DeleteFirstname", "DeleteName");
        customer.setAddress(new Address("DeleteStreet", "DeletePostZip", "DeleteCity", "DeleteCountry"));
        Customer toDeleteCustomer = boundary.saveCustomer(customer);
        final long customersNr = toDeleteCustomer.getNumber();
        assertTrue(customersNr > 0);

        //Pruefen ob der Customer in der Liste hinzugefuegt wurde und anschliessend loeschen
        assertTrue(boundary.getCustomerByNr(customersNr).isPresent());
        assertTrue(boundary.deleteCustomer(customersNr));

        //Loeschen eines Customer der nicht existiert
        assertFalse(boundary.getCustomerByNr(customersNr).isPresent());
        assertFalse(boundary.deleteCustomer(customersNr));


    }

    @Test(expected = RentCarValidationException.class)
    public void saveCustomerWithNullValueOnAddress() {
        Address address = new Address("strasse", "8000", "city", null);
        Customer customer = new Customer("firstname", "name");
        customer.setAddress(address);
        boundary.saveCustomer(customer);

    }

    @Test(expected = RentCarValidationException.class)
    public void saveCustomerWithNullValueOnName() {
        Address address = new Address("strasse", "8000", "city", "CH");
        Customer customer = new Customer("firstname",null);
        customer.setAddress(address);
        boundary.saveCustomer(customer);

    }
}