use `CARRENT`;

# Export von Tabelle ADDRESS
# ------------------------------------------------------------
INSERT INTO `ADDRESS` (`ID`, `CITY`, `COUNTRY`, `POST_ZIP`, `STREET`)
VALUES
	(1,'St. Gallen','CH','9000','Wildeggstrasse 32'),
	(2,'Amriswil','CH','8580','Hellmühlestr. 24a'),
	(3,'Aarau','CH','5000','Alpsteinstrasse 12'),
	(4,'Biberstein','CH','5023','Alte Wollerauerstrasse 15'),
	(5,'Buchs AG','CH','5033','Altwiesenstrasse 274'),
	(6,'Suhr','CH','5034','Altwiesenstrasse 76'),
	(7,'Unterentfelden','CH','5035','Am Geerenrain 11'),
	(8,'Frick','CH','5070','Am Stiegweg 14b'),
	(9,'Rupperswil','CH','5102','Am Wildbach 5'),
	(10,'Möriken AG','CH','5103','Aspenrüti'),
	(11,'Umiken','CH','5222','Aspstrasse 46'),
	(12,'Tegerfelden','CH','5306','Bachfeldstrasse 4a'),
	(13,'Gippingen','CH','5316','Bächlistrasse 9'),
	(14,'Gebenstorf','CH','5412','Badenerstrasse 11'),
	(15,'Nussbaumen AG','CH','5415','Bahnhofstrasse 118'),
	(16,'Untersiggenthal','CH','5417','Bahnhofstrasse 42'),
	(17,'Neuenhof','CH','5432','Beichlen 1'),
	(18,'Würenlos','CH','5436','Bergstrasse 33'),
	(19,'Mellingen','CH','5507','Bildstrasse 2a'),
	(20,'Buttwil','CH','5632','Birmensdorferstr. 35'),
	(21,'Dürrenäsch','CH','5724','Birmensdorferstrasse 32'),
	(22,'Altishofen','CH','6246','Bleichiweg 7c'),
	(23,'Roggliswil','CH','6265','Bleulerstrasse 3'),
	(24,'Chur','CH','7000','Blumentalstrasse 20'),
	(25,'Zizers','CH','7205','Bochslenstrasse 14'),
	(26,'Igis','CH','7206','Bodenfeldstrasse 7'),
	(27,'Malans GR','CH','7208','Brändlihangstrasse 1b'),
	(28,'Klosters','CH','7250','Brandstrasse 27'),
	(29,'Landquart','CH','7302','Bruderhöflistrasse 38'),
	(30,'Sargans','CH','7320','Bruggerstrasse 25')
;


# Export von Tabelle CUSTOMER
# ------------------------------------------------------------
INSERT INTO `CUSTOMER` (`ID`, `FIRSTNAME`, `NAME`, `NUMBER`, `ADDRESS_ID`)
VALUES
  (1,'Stefan','Allenspach',1,1),
  (2,'Cyrill','Schwyter',2,2),
  (3,'Alexander','Althaus',3,3),
  (4,'Alfred','Ambrosini',4,4),
  (5,'Andreas','Anderes',5,5),
  (6,'Andreas','Andri',6,6),
  (7,'Angelika','Arnitz',7,7),
  (8,'Antonio','Badertscher',8,8),
  (9,'Antonio','Baracchi',9,9),
  (10,'Beat','Beurer',10,10),
  (11,'Benno','Bieri',11,11),
  (12,'Cassiano','Bircher',12,12),
  (13,'Cesar','Blaser',13,13),
  (14,'Christian','Blum',14,14),
  (15,'Christian','Bollhalder',15,15),
  (16,'Daniel','Bollinger',16,16),
  (17,'Daniel','Bonorand',17,17),
  (18,'Daniel','Boos',18,18),
  (19,'Daniel','Bösch',19,19),
  (20,'Daniel','Bosshard',20,20),
  (21,'Denis','Brüngger',21,21),
  (22,'Dominik','Brunner',22,22),
  (23,'Egon','Brütsch',23,23),
  (24,'Enrico','Büchli',24,24),
  (25,'Ernst','Burri',25,25),
  (26,'Ernst','Carnier',26,26),
  (27,'Erwin','Castelo',27,27),
  (28,'Erwin','Christen',28,28),
  (29,'Erwin','Cicerchia',29,29),
  (30,'Florian','Schwyter',30,30)
;
