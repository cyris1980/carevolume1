use carrent;
# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.6.34)
# Datenbank: carrent
# Erstellt am: 2017-07-29 14:22:57 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Export von Tabelle ADDRESS
# ------------------------------------------------------------

LOCK TABLES `ADDRESS` WRITE;
/*!40000 ALTER TABLE `ADDRESS` DISABLE KEYS */;

INSERT INTO `ADDRESS` (`ID`, `CITY`, `COUNTRY`, `POST_ZIP`, `STREET`)
VALUES
	(1,'St. Gallen','CH','9000','Wildeggstrasse 32'),
	(2,'Amriswil','CH','8580','Hellmühlestr. 24a'),
	(3,'Aarau','CH','5000','Alpsteinstrasse 12'),
	(4,'Biberstein','CH','5023','Alte Wollerauerstrasse 15'),
	(5,'Buchs AG','CH','5033','Altwiesenstrasse 274'),
	(6,'Suhr','CH','5034','Altwiesenstrasse 76'),
	(7,'Unterentfelden','CH','5035','Am Geerenrain 11'),
	(8,'Frick','CH','5070','Am Stiegweg 14b'),
	(9,'Rupperswil','CH','5102','Am Wildbach 5'),
	(10,'Möriken AG','CH','5103','Aspenrüti'),
	(11,'Umiken','CH','5222','Aspstrasse 46'),
	(12,'Tegerfelden','CH','5306','Bachfeldstrasse 4a'),
	(13,'Gippingen','CH','5316','Bächlistrasse 9'),
	(14,'Gebenstorf','CH','5412','Badenerstrasse 11'),
	(15,'Nussbaumen AG','CH','5415','Bahnhofstrasse 118'),
	(16,'Untersiggenthal','CH','5417','Bahnhofstrasse 42'),
	(17,'Neuenhof','CH','5432','Beichlen 1'),
	(18,'Würenlos','CH','5436','Bergstrasse 33'),
	(19,'Mellingen','CH','5507','Bildstrasse 2a'),
	(20,'Buttwil','CH','5632','Birmensdorferstr. 35'),
	(21,'Dürrenäsch','CH','5724','Birmensdorferstrasse 32'),
	(22,'Altishofen','CH','6246','Bleichiweg 7c'),
	(23,'Roggliswil','CH','6265','Bleulerstrasse 3'),
	(24,'Chur','CH','7000','Blumentalstrasse 20'),
	(25,'Zizers','CH','7205','Bochslenstrasse 14'),
	(26,'Igis','CH','7206','Bodenfeldstrasse 7'),
	(27,'Malans GR','CH','7208','Brändlihangstrasse 1b'),
	(28,'Klosters','CH','7250','Brandstrasse 27'),
	(29,'Landquart','CH','7302','Bruderhöflistrasse 38'),
	(30,'Sargans','CH','7320','Bruggerstrasse 25');

/*!40000 ALTER TABLE `ADDRESS` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle CAR
# ------------------------------------------------------------

LOCK TABLES `CAR` WRITE;
/*!40000 ALTER TABLE `CAR` DISABLE KEYS */;

INSERT INTO `CAR` (`ID`, `BRAND`, `IDENTIFICATION`, `TYP`, `CARCLASS_ID`)
VALUES
	(1,'VW','CR-0001','Kleinwagen',1),
	(2,'VW','CR-0002','Kombi',1),
	(3,'Skoda','CR-0003','Kleinwagen',1),
	(4,'Skoda','CR-0004','Kombi',1),
	(5,'Peugeot','CR-0005','Kleinwagen',1),
	(6,'Peugeot','CR-0006','Kombi',1),
	(7,'VW','CR-0007','Limusine',2),
	(8,'WV','CR-0008','Kombi',2),
	(9,'Audi','CR-0009','Limusine',2),
	(10,'Audi','CR-0010','Kombi',2),
	(11,'BMW','CR-0011','Limusine',2),
	(12,'BMW','CR-0012','Kombi',2),
	(13,'Audi','CR-0013','Limusine',3),
	(14,'Audi','CR-0014','Sportwagen',3),
	(15,'BMW','CR-0015','Limusine',3),
	(16,'BMW','CR-0016','Sportwagen',3),
	(17,'Lexus','CR-0017','Limusine',3),
	(18,'Lexus','CR-0018','Sportwagen',3);

/*!40000 ALTER TABLE `CAR` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle CARCLASS
# ------------------------------------------------------------

LOCK TABLES `CARCLASS` WRITE;
/*!40000 ALTER TABLE `CARCLASS` DISABLE KEYS */;

INSERT INTO `CARCLASS` (`ID`, `DAYRATE`, `NAME`)
VALUES
	(1,100.00,'Einfachklasse'),
	(2,200.00,'Mittelklasse'),
	(3,300.00,'Luxusklasse');

/*!40000 ALTER TABLE `CARCLASS` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle CUSTOMER
# ------------------------------------------------------------

LOCK TABLES `CUSTOMER` WRITE;
/*!40000 ALTER TABLE `CUSTOMER` DISABLE KEYS */;

INSERT INTO `CUSTOMER` (`ID`, `FIRSTNAME`, `NAME`, `NUMBER`, `ADDRESS_ID`)
VALUES
	(1,'Stefan','Allenspach',1,1),
	(2,'Cyrill','Schwyter',2,2),
	(3,'Alexander','Althaus',3,3),
	(4,'Alfred','Ambrosini',4,4),
	(5,'Andreas','Anderes',5,5),
	(6,'Andreas','Andri',6,6),
	(7,'Angelika','Arnitz',7,7),
	(8,'Antonio','Badertscher',8,8),
	(9,'Antonio','Baracchi',9,9),
	(10,'Beat','Beurer',10,10),
	(11,'Benno','Bieri',11,11),
	(12,'Cassiano','Bircher',12,12),
	(13,'Cesar','Blaser',13,13),
	(14,'Christian','Blum',14,14),
	(15,'Christian','Bollhalder',15,15),
	(16,'Daniel','Bollinger',16,16),
	(17,'Daniel','Bonorand',17,17),
	(18,'Daniel','Boos',18,18),
	(19,'Daniel','Bösch',19,19),
	(20,'Daniel','Bosshard',20,20),
	(21,'Denis','Brüngger',21,21),
	(22,'Dominik','Brunner',22,22),
	(23,'Egon','Brütsch',23,23),
	(24,'Enrico','Büchli',24,24),
	(25,'Ernst','Burri',25,25),
	(26,'Ernst','Carnier',26,26),
	(27,'Erwin','Castelo',27,27),
	(28,'Erwin','Christen',28,28),
	(29,'Erwin','Cicerchia',29,29),
	(30,'Florian','Schwyter',30,30);

/*!40000 ALTER TABLE `CUSTOMER` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle RESERVATION
# ------------------------------------------------------------

LOCK TABLES `RESERVATION` WRITE;
/*!40000 ALTER TABLE `RESERVATION` DISABLE KEYS */;

INSERT INTO `RESERVATION` (`ID`, `ENDDATE`, `RESERVATIONNUMBER`, `STARTDATE`, `CAR_ID`, `CARCLASS_ID`, `CUSTOMER_ID`)
VALUES
	(1,'2017-01-11',1,'2017-01-01',NULL,1,1),
	(2,'2017-02-12',2,'2017-02-01',NULL,2,2),
	(3,'2017-03-13',3,'2017-03-01',NULL,3,3),
	(4,'2017-04-14',4,'2017-04-01',NULL,1,4),
	(5,'2017-05-15',5,'2017-05-01',NULL,2,5),
	(6,'2017-06-16',6,'2017-06-01',NULL,3,6),
	(7,'2017-07-17',7,'2017-07-01',6,1,7),
	(8,'2017-08-18',8,'2017-08-01',12,2,8),
	(9,'2017-09-19',9,'2017-09-01',18,3,9);

/*!40000 ALTER TABLE `RESERVATION` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
