-- Info: Export dem Programm SQLPro (Information).
drop schema if exists `CARRENT`;
create schema if not exists `CARRENT`;
use `CARRENT`;

CREATE TABLE ADDRESS (ID BIGINT AUTO_INCREMENT NOT NULL, CITY VARCHAR(80), COUNTRY VARCHAR(80), POST_ZIP VARCHAR(40), STREET VARCHAR(80), PRIMARY KEY (ID));
CREATE TABLE CUSTOMER (ID BIGINT AUTO_INCREMENT NOT NULL, FIRSTNAME VARCHAR(255), NAME VARCHAR(255), NUMBER BIGINT, ADDRESS_ID BIGINT, PRIMARY KEY (ID));
CREATE TABLE CARCLASS (ID BIGINT AUTO_INCREMENT NOT NULL, DAYRATE DECIMAL(11,2), NAME VARCHAR(255), PRIMARY KEY (ID));
CREATE TABLE CAR (ID BIGINT AUTO_INCREMENT NOT NULL, BRAND VARCHAR(255), IDENTIFICATION VARCHAR(255), TYP VARCHAR(255), CARCLASS_ID BIGINT, PRIMARY KEY (ID));
CREATE TABLE RESERVATION (ID BIGINT AUTO_INCREMENT NOT NULL, ENDDATE DATE, RESERVATIONNUMBER BIGINT, STARTDATE DATE, CAR_ID BIGINT, CARCLASS_ID BIGINT, CUSTOMER_ID BIGINT, PRIMARY KEY (ID));
ALTER TABLE CUSTOMER ADD CONSTRAINT FK_CUSTOMER_ADDRESS_ID FOREIGN KEY (ADDRESS_ID) REFERENCES ADDRESS (ID);
ALTER TABLE CAR ADD CONSTRAINT FK_CAR_CARCLASS_ID FOREIGN KEY (CARCLASS_ID) REFERENCES CARCLASS (ID);
ALTER TABLE RESERVATION ADD CONSTRAINT FK_RESERVATION_CUSTOMER_ID FOREIGN KEY (CUSTOMER_ID) REFERENCES CUSTOMER (ID);
ALTER TABLE RESERVATION ADD CONSTRAINT FK_RESERVATION_CAR_ID FOREIGN KEY (CAR_ID) REFERENCES CAR (ID);
ALTER TABLE RESERVATION ADD CONSTRAINT FK_RESERVATION_CARCLASS_ID FOREIGN KEY (CARCLASS_ID) REFERENCES CARCLASS (ID);


# Export von Tabelle ADDRESS
# ------------------------------------------------------------

# DROP TABLE IF EXISTS `ADDRESS`;
#
# CREATE TABLE `ADDRESS` (
#   `ID` bigint(20) NOT NULL AUTO_INCREMENT,
#   `CITY` varchar(80) DEFAULT NULL,
#   `COUNTRY` varchar(80) DEFAULT NULL,
#   `POST_ZIP` varchar(40) DEFAULT NULL,
#   `STREET` varchar(80) DEFAULT NULL,
#   PRIMARY KEY (`ID`)
# ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
#
#
#
# # Export von Tabelle CUSTOMER
# # ------------------------------------------------------------
#
# DROP TABLE IF EXISTS `CUSTOMER`;
#
# CREATE TABLE `CUSTOMER` (
#   `ID` bigint(20) NOT NULL AUTO_INCREMENT,
#   `FIRSTNAME` varchar(255) DEFAULT NULL,
#   `NAME` varchar(255) DEFAULT NULL,
#   `NUMBER` int(11) DEFAULT NULL,
#   `ADDRESS_ID` bigint(20) DEFAULT NULL,
#   PRIMARY KEY (`ID`),
#   UNIQUE KEY `NUMBER` (`NUMBER`),
#   KEY `FK_CUSTOMER_ADDRESS_ID` (`ADDRESS_ID`),
#   CONSTRAINT `FK_CUSTOMER_ADDRESS_ID` FOREIGN KEY (`ADDRESS_ID`) REFERENCES `ADDRESS` (`ID`)
# ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
#
#
# # Export von Tabelle CARCLASS
# # ------------------------------------------------------------
#
# DROP TABLE IF EXISTS `CARCLASS`;
#
# CREATE TABLE CARCLASS (
#   ID BIGINT AUTO_INCREMENT NOT NULL,
#   DAYRATE DECIMAL(11,2) NOT NULL,
#   NAME VARCHAR(255) NOT NULL UNIQUE,
#   PRIMARY KEY (ID)
# ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
#
#
# # Export von Tabelle CAR
# # ------------------------------------------------------------
#
# DROP TABLE IF EXISTS `CAR`;
#
# CREATE TABLE `CAR` (
#   `ID` bigint(20) NOT NULL AUTO_INCREMENT,
#   `BRAND` varchar(255) NOT NULL,
#   `IDENFIFICATION` varchar(255) NOT NULL,
#   `TYP` varchar(255) NOT NULL,
#   `CARCLASS_ID` bigint(20) NOT NULL,
#   PRIMARY KEY (`ID`),
#   UNIQUE KEY `IDENFIFICATION` (`IDENFIFICATION`),
#   KEY `FK_CAR_CARCLASS_ID` (`CARCLASS_ID`),
#   CONSTRAINT `FK_CAR_CARCLASS_ID` FOREIGN KEY (`CARCLASS_ID`) REFERENCES `CARCLASS` (`ID`)
# ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
