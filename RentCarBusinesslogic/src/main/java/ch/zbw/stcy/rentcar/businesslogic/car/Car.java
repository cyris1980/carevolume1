package ch.zbw.stcy.rentcar.businesslogic.car;

import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClass;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;

/**
 * Created by stefanallenspach on 03.06.17.
 */
@Entity
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    private long id;

    @Column(name = "IDENTIFICATION", length = 255)
    private String identification;

    @Column(name = "BRAND", length = 255)
    private String brand;

    @Column(name = "TYP", length = 255)
    private String typ;

    @ManyToOne
    @JoinColumn(name = "CARCLASS_ID")
    private CarClass carClass;


    public Car() {
    }

    public Car(String identification, String brand, String typ, CarClass carClass) {
        this.setIdentification(identification);
        this.setBrand(brand);
        this.setTyp(typ);
        this.setCarClass(carClass);
    }

    public long getId() {
        return id;
    }


    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) throws IllegalArgumentException {
        if (this.identification != null && !this.identification.equals(identification)) {
            throw new IllegalArgumentException("identification can not be overwritten");
        }
        this.identification = identification;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public CarClass getCarClass() {
        return carClass;
    }

    public void setCarClass(CarClass carClass) {
        this.carClass = carClass;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        return new EqualsBuilder()
                .append(identification, car.identification)
                .append(brand, car.brand)
                .append(typ, car.typ)
                .append(carClass, car.carClass)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(identification)
                .append(brand)
                .append(typ)
                .append(carClass)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("identification", identification)
                .append("brand", brand)
                .append("typ", typ)
                .append("carClass", carClass)
                .toString();
    }

    /**
     * Uebernimmt alle Werte des ohter Objekts
     * Wird gebraucht um das Objekt aus der Datenbank mit den neuen Werten upzudaten.
     * Ansonstent meusste die Tabellen id gesetzt werden. Diesr sollte aber nur von der DB gesetzt werden.
     *
     * @param other Objekt mit den neuen Werten
     * @throws IllegalArgumentException Wenn die Indentification mit einem anderen Wert ueberschrieben wird
     * @throws IllegalArgumentException Wenn Car null ist
     */
    public void update(Car other) throws IllegalArgumentException {
        if(other == null){
            throw new IllegalArgumentException("car is null");
        }
        setIdentification(other.getIdentification());
        setBrand(other.getBrand());
        setCarClass(other.getCarClass());
        setTyp(other.getTyp());
    }

    public boolean isValid() throws IllegalArgumentException {
        StringBuilder errorMessage = new StringBuilder();

        if (identification == null) {
            errorMessage.append("identification is null;");
        } else if (identification.equals("")) {
            errorMessage.append(("identification is empty;"));
        }

        if (brand == null) {
            errorMessage.append("brand is null;");
        } else if (brand.equals("")) {
            errorMessage.append(("brand is empty;"));
        }

        if (typ == null) {
            errorMessage.append("typ is null;");
        } else if (typ.equals("")) {
            errorMessage.append(("typ is empty;"));
        }

        if (carClass == null) {
            errorMessage.append("carClass is null;");
        }
        if (errorMessage.length() > 0) {
            throw new IllegalArgumentException(errorMessage.toString());
        }
        return true;
    }
}
