package ch.zbw.stcy.rentcar.businesslogic.carClass;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by stefanallenspach on 03.06.17.
 */
@Entity
public class CarClass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    private long id;

    @Column(name = "DAYRATE", precision = 11, scale = 2)
    private BigDecimal dayRate;

    @Column(name = "NAME", length = 255)
    private String name;

    public CarClass() {
    }

    public CarClass(BigDecimal dayRate, String name) {
        this.setDayRate(dayRate);
        this.setName(name);
    }

    public long getId() {
        return id;
    }

    public BigDecimal getDayRate() {
        return dayRate;
    }

    public void setDayRate(BigDecimal dayRate) {
        this.dayRate = dayRate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (this.name != null && !this.name.equals(name)) {
            throw new IllegalArgumentException("CarClass name can not be overwritten");
        }
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        CarClass carClass = (CarClass) o;

        return new EqualsBuilder()
                .append(dayRate, carClass.dayRate)
                .append(name, carClass.name)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(dayRate)
                .append(name)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("dayRate", dayRate)
                .append("name", name)
                .toString();
    }

    /**
     * Uebernimmt alle Werte des ohter Objekts
     * Wird gebraucht um das Objekt aus der Datenbank mit den neuen Werten upzudaten.
     * Ansonstent meusste die Tabellen id gesetzt werden. Diesr sollte aber nur von der DB gesetzt werden.
     *
     * @param other Objekt mit den neuen Werten
     */

    public void update(CarClass other) throws IllegalArgumentException {
        if (other == null) {
            throw new IllegalArgumentException("carClass is null");
        }
        setName(other.getName());
        setDayRate(other.getDayRate());
    }

    public boolean isValid() throws IllegalArgumentException {
        StringBuilder errorMessage = new StringBuilder();

        if (name == null) {
            errorMessage.append("name is null;");
        } else if (name.equals("")) {
            errorMessage.append(("name is empty;"));
        }

        if (dayRate == null) {
            errorMessage.append("dayRate is null;");
        } else if (dayRate.doubleValue() <= 0) {
            errorMessage.append(("dayRate is less than 0;"));
        }

        if (errorMessage.length() > 0) {
            throw new IllegalArgumentException(errorMessage.toString());
        }
        return true;
    }
}
