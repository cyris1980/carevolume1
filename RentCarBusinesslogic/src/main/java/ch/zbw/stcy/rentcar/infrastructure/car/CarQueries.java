package ch.zbw.stcy.rentcar.infrastructure.car;

/**
 * Created by stefanallenspach on 07.07.17.
 */
public class CarQueries {

    public final static String CAR_FIND_ALL 			    = "Car.findAll";
    public final static String CAR_FIND_BY_IDENT 		    = "Car.findByIdentification";
    public final static String CAR_FIND_BY_CAR_CLASS        = "Car.findByCarClass";
    public final static String CAR_DELETE_BY_IDENT 	        = "Car.deleteByIdentification";
}
