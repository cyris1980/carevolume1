package ch.zbw.stcy.rentcar.businesslogic.carClass;

import java.util.List;
import java.util.Optional;

/**
 * Created by stefanallenspach on 07.07.17.
 */
public interface ICarClassBoundary {

    /**
     * Speichert einen nocht nicht vorhandene CarClass
     *
     * @param carClass zu speichdernte CarClass
     * @return gespeicherte CarClass
     * @throws IllegalArgumentException falls CarClass bereits existiert
     */
    CarClass saveCarClass(CarClass carClass) throws IllegalArgumentException;

    /**
     *
     * Updaten eine vorhandene CarClass
     *
     * @param carClass zu updatende CarClass
     * @return updatetede CarClass
     * @throws IllegalArgumentException Falls CarClass nicht vorhanden
     */
    CarClass updateCarClass(CarClass carClass) throws IllegalArgumentException;

    List<CarClass> getAllCarClasses();

    boolean deleteCarClass(CarClass carClass);

    Optional<CarClass> getCarClassByName(String name);

    boolean deleteCarClassByName(String name);

}
