package ch.zbw.stcy.rentcar.infrastructure;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by cyrill on 26.05.17.
 */
public interface IRepository<E> {

    public E saveEntity(E entity);

    public boolean deleteEntity(E entity);

    public Optional<E> getEntity(String querry);

    public Optional<E> getEntity(String querry, Map<String, Object> params);

    public List<E> getEntityList(String querry);

    public List<E> getEntityList(String querry, Map<String, Object> params);

    public Object getSingleResult(String querry);

    public Object getSingleResult(String querry, Map<String, Object> params);

    public List<Object> getResultList(String querry);

    public List<Object> getResultList(String querry, Map<String, Object> params);
}
