package ch.zbw.stcy.rentcar.infrastructure.customer;

/**
 * Created by cyrill on 27.05.17.
 */
public class CustomerQueries {

    public final static String CUSTOMER_FIND_ALL 			= "Customer.findAll";
    public final static String CUSTOMER_FIND_BY_NUMBER 		= "Customer.findByNr";
    public final static String CUSTOMER_FIND_BY_NAME        = "Customer.findByName";
    public final static String CUSTOMER_SEARCH              = "Customer.search";
    public final static String CUSTOMER_DELETE_BY_NUMBER 	= "Customer.deleteByNr";
}
