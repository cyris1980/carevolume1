package ch.zbw.stcy.rentcar.infrastructure;

import ch.zbw.stcy.rentcar.businesslogic.car.Car;
import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClass;
import ch.zbw.stcy.rentcar.businesslogic.customer.Customer;
import ch.zbw.stcy.rentcar.businesslogic.reservation.Reservation;
import ch.zbw.stcy.rentcar.infrastructure.car.CarController;
import ch.zbw.stcy.rentcar.infrastructure.car.ICarController;
import ch.zbw.stcy.rentcar.infrastructure.carClass.CarClassController;
import ch.zbw.stcy.rentcar.infrastructure.carClass.ICarClassController;
import ch.zbw.stcy.rentcar.infrastructure.customer.CustomerController;
import ch.zbw.stcy.rentcar.infrastructure.customer.ICustomerController;
import ch.zbw.stcy.rentcar.infrastructure.reservation.IReservationController;
import ch.zbw.stcy.rentcar.infrastructure.reservation.ReservationController;

/**
 * Created by cyrill on 26.05.17.
 */
public class RepositoryFactory {

    private static String PERSISTENT_UNIT = "RentCar";

    public static ICustomerController createCustomerDBOperations() {
        IRepository<Customer> customerDBConnection = new Repository<>(Customer.class,
                new DataBaseConnection(PERSISTENT_UNIT));
        return new CustomerController(customerDBConnection);
    }

    public static ICarClassController createCarClassDBOperations() {
        IRepository<CarClass> carClassDBConnection = new Repository<>(CarClass.class,
                new DataBaseConnection(PERSISTENT_UNIT));
        return new CarClassController(carClassDBConnection);
    }

    public static ICarController createCarDBOperations() {
        IRepository<Car> carDBConnection = new Repository<>(Car.class,
                new DataBaseConnection(PERSISTENT_UNIT));
        return new CarController(carDBConnection);
    }

    public static IReservationController createReservationDBOperations() {
        IRepository<Reservation> reservationDBConnection = new Repository<>(Reservation.class,
                new DataBaseConnection(PERSISTENT_UNIT));
        return new ReservationController(reservationDBConnection);
    }
}
