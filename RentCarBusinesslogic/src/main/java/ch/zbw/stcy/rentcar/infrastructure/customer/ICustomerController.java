package ch.zbw.stcy.rentcar.infrastructure.customer;

import ch.zbw.stcy.rentcar.businesslogic.customer.Customer;

import java.util.List;
import java.util.Optional;

/**
 * Created by cyrill on 26.05.17.
 */
public interface ICustomerController {

    public Customer saveCustomer(Customer customer);

    public List<Customer> getAllCustomers();

    public List<Customer> searchCustomers(String searchString);

    public Optional<Customer> getCustomerByNr(long number);

    public List<Customer> getCustomersByName(String name);

    public boolean deleteCustomer(long customerNr);
}
