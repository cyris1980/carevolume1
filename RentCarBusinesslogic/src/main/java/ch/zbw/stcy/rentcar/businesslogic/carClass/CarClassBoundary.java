package ch.zbw.stcy.rentcar.businesslogic.carClass;

import ch.zbw.stcy.rentcar.infrastructure.carClass.ICarClassController;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Created by stefanallenspach on 07.07.17.
 */
public class CarClassBoundary implements ICarClassBoundary {
    private ICarClassController controller;

    public CarClassBoundary(ICarClassController carClassController) {
        this.controller = carClassController;
    }

    @Override
    public List<CarClass> getAllCarClasses() {
        return controller.getAllCarClasses();
    }


    @Override
    public CarClass saveCarClass(CarClass carClass) throws IllegalArgumentException {
        if (carClass == null) throw new IllegalArgumentException("car is null");
        if (controller.getCarClassByName(carClass.getName()).isPresent()) {
            throw new IllegalArgumentException("carClass already in list");
        } else {
            carClass.isValid();
            return controller.saveCarClass(carClass);
        }
    }

    @Override
    public CarClass updateCarClass(CarClass carClass) throws IllegalArgumentException {
        if (carClass == null) throw new IllegalArgumentException("car is null");
        CarClass carClassDB = controller.getCarClassByName(carClass.getName())
                .orElseThrow(() -> new NoSuchElementException("carClass not in list"));

        carClassDB.update(carClass);
        carClassDB.isValid();
        return controller.saveCarClass(carClassDB);
    }

    @Override
    public boolean deleteCarClass(CarClass carClass) {
        if (carClass == null) throw new IllegalArgumentException("car is null");
        return controller.deleteCarClass(carClass);
    }

    @Override
    public Optional<CarClass> getCarClassByName(String name) {
        return controller.getCarClassByName(name);
    }

    @Override
    public boolean deleteCarClassByName(String name) {
        return controller.deleteCarClassByName(name);
    }

}
