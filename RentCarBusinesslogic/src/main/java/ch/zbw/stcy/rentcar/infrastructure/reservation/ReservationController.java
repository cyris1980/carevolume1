package ch.zbw.stcy.rentcar.infrastructure.reservation;

import ch.zbw.stcy.rentcar.businesslogic.customer.Customer;
import ch.zbw.stcy.rentcar.businesslogic.reservation.Reservation;
import ch.zbw.stcy.rentcar.infrastructure.IRepository;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by stefanallenspach on 17.07.17.
 */
public class ReservationController implements IReservationController {

    private IRepository<Reservation> repository;

    public ReservationController(IRepository<Reservation> repository) {
        this.repository = repository;
    }

    @Override
    public Reservation saveReservation(Reservation reservation) {
        return repository.saveEntity(reservation);
    }

    @Override
    public List<Reservation> getAllReservation() {
        return repository.getEntityList(ReservationQueries.RESERVATION_FIND_ALL);
    }

    @Override
    public List<Reservation> searchAllReservationFromCustomer(Customer customer) {
        Map<String, Object> params = new HashMap<>();
        params.put("customer", customer);
        return repository.getEntityList(ReservationQueries.RESERVATION_FIND_ALL_FROM_CUSTOMER, params);
    }

    @Override
    public Optional<Reservation> getReservationByNr(long reservationNumber) {
        Map<String, Object> params = new HashMap<>();
        params.put("reservationNumber", reservationNumber);
        return repository.getEntity(ReservationQueries.RESERVATION_FIND_BY_NUMBER, params);
    }

    @Override
    public List<Reservation> getReservationByStartDate(LocalDate startDate) {
        Map<String, Object> params = new HashMap<>();
        params.put("startDate", startDate);
        return repository.getEntityList(ReservationQueries.RESERVATION_FIND_BY_DATE, params);
    }

    @Override
    public boolean deleteReservation(long reservationNumber) {
        Map<String, Object> params = new HashMap<>();
        params.put("reservationNumber", reservationNumber);
        Optional<Reservation> opReservation = repository.getEntity(ReservationQueries.RESERVATION_FIND_BY_NUMBER, params);
        if (opReservation.isPresent()) {
            repository.deleteEntity(opReservation.get());
            return true;
        } else {
            return false;
        }
    }
}
