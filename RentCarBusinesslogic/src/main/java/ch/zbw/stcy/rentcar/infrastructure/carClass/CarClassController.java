package ch.zbw.stcy.rentcar.infrastructure.carClass;

import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClass;
import ch.zbw.stcy.rentcar.businesslogic.customer.Customer;
import ch.zbw.stcy.rentcar.infrastructure.IRepository;
import ch.zbw.stcy.rentcar.infrastructure.customer.CustomerQueries;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by stefanallenspach on 09.07.17.
 */
public class CarClassController implements ICarClassController {
    private IRepository<CarClass> repository;

    public CarClassController(IRepository<CarClass> repository) {
        this.repository = repository;
    }

    @Override
    public List<CarClass> getAllCarClasses() {
        return repository.getEntityList(CarClassQueries.CARCLASS_FIND_ALL);
    }

    @Override
    public CarClass saveCarClass(CarClass carClass) {
        return repository.saveEntity(carClass);
    }

    @Override
    public boolean deleteCarClass(CarClass carClass) {
        return repository.deleteEntity(carClass);
    }

    @Override
    public Optional<CarClass> getCarClassByName(String name) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        return repository.getEntity(CarClassQueries.CARCLASS_FIND_BY_NAME, params);
    }

    @Override
    public boolean deleteCarClassByName(String name) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        Optional<CarClass> opCarClass = repository.getEntity(CarClassQueries.CARCLASS_FIND_BY_NAME, params);
        if (opCarClass.isPresent()) {
            repository.deleteEntity(opCarClass.get());
            return true;
        } else
            return false;
    }


}
