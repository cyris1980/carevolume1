package ch.zbw.stcy.rentcar.infrastructure;

public class RentCarValidationException extends RuntimeException
{
    public RentCarValidationException(String message) {
        super(message);
    }
}
