package ch.zbw.stcy.rentcar.infrastructure;

import ch.zbw.stcy.rentcar.infrastructure.commons.Utils;

import javax.persistence.*;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by cyrill on 26.05.17.
 */
public class Repository<E> implements IRepository<E> {

    private final Class<E> type;
    private final IDataBaseConnection dbConnection;

    public Repository(Class<E> type, IDataBaseConnection dbConnection) {
        this.type = type;
        this.dbConnection = dbConnection;
    }

    @Override
    public Optional<E> getEntity(String querry) throws RepositoryException {
        try {
            EntityManager entityManager = dbConnection.getEntityManager();
            TypedQuery<E> tq = entityManager.createNamedQuery(querry, type);

            E result = tq.getSingleResult();

            return Optional.of(result);
        } catch (NoResultException e) {
            return Optional.empty();
        } catch (Exception exeption) {
            throw new RepositoryException(exeption.getMessage(), exeption.getCause());

        }
    }

    @Override
    public Optional<E> getEntity(String querry, Map<String, Object> params) throws RepositoryException {
        try {

            EntityManager entityManager = dbConnection.getEntityManager();
            TypedQuery<E> tq = entityManager.createNamedQuery(querry, type);

            params.forEach((key, value) -> tq.setParameter(key, value));
            E result = tq.getSingleResult();
            return Optional.of(result);
        } catch (NoResultException e) {
            return Optional.empty();
        } catch (Exception exeption) {
            throw new RepositoryException(exeption.getMessage(), exeption.getCause());

        }
    }
    @Override
    public List<E> getEntityList(String querry) throws RepositoryException {
        try {
            EntityManager entityManager = dbConnection.getEntityManager();
            TypedQuery<E> tq = entityManager.createNamedQuery(querry, type);
            return tq.getResultList();
        } catch (Exception e) {
            throw new RepositoryException(e.getMessage(), e.getCause());
        }

    }

    @Override
    public E saveEntity(E entity) throws RepositoryException, RentCarValidationException {
        try {
            EntityManager entityManger = dbConnection.getEntityManager();
            EntityTransaction transaction = entityManger.getTransaction();
            transaction.begin();
            E result = entityManger.merge(entity);
            transaction.commit();
            return result;

        }
        catch(ConstraintViolationException violationException){

            Iterator<ConstraintViolation<?>> iterator = violationException.getConstraintViolations().iterator();

            throw new RentCarValidationException(Utils.formatConstraintViolationsToErrorMessage(iterator));
        }
        catch (RollbackException rollbackException){
            Throwable cause = rollbackException.getCause();
            if(cause.getClass().equals(ConstraintViolationException.class)){
                ConstraintViolationException constraintViolation = (ConstraintViolationException) cause;
                Iterator<ConstraintViolation<?>> iterator = constraintViolation.getConstraintViolations().iterator();
                throw new RentCarValidationException(Utils.formatConstraintViolationsToErrorMessage(iterator));
            }
            else{
                throw rollbackException;
            }
        }
        catch (Exception e) {
            throw new RepositoryException(e.getMessage(), e.getCause());
        }
    }

    @Override
    public boolean deleteEntity(E entity) throws RepositoryException {
        try {
            EntityManager entityManger = dbConnection.getEntityManager();
            EntityTransaction transaction = entityManger.getTransaction();
            transaction.begin();
            entity = entityManger.merge(entity);
            entityManger.remove(entity);
            transaction.commit();
            return true;
        } catch (Exception e) {
            throw new RepositoryException(e.getMessage(), e.getCause());
        }
    }

    @Override
    public List<E> getEntityList(String querry, Map<String, Object> params) throws RepositoryException {
        try {
            EntityManager entityManager = dbConnection.getEntityManager();
            TypedQuery<E> tq = entityManager.createNamedQuery(querry, type);
            params.forEach((key, value) -> tq.setParameter(key, value));
            return tq.getResultList();
        } catch (Exception e) {
            throw new RepositoryException(e.getMessage(), e.getCause());
        }

    }

    @Override
    public Object getSingleResult(String querry) throws RepositoryException {
        try {
            EntityManager entityManager = dbConnection.getEntityManager();
            Query tq = entityManager.createNamedQuery(querry);
            return tq.getSingleResult();
        } catch (Exception e) {
            throw new RepositoryException(e.getMessage(), e.getCause());
        }
    }

    @Override
    public Object getSingleResult(String querry, Map<String, Object> params) throws RepositoryException {
        try {
            EntityManager entityManager = dbConnection.getEntityManager();
            Query tq = entityManager.createNamedQuery(querry);
            params.forEach((key, value) -> tq.setParameter(key, value));
            return tq.getSingleResult();
        } catch (Exception e) {
            throw new RepositoryException(e.getMessage(), e.getCause());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Object> getResultList(String querry) throws RepositoryException {
        try {
            EntityManager entityManager = dbConnection.getEntityManager();
            Query tq = entityManager.createNamedQuery(querry);
            return tq.getResultList();
        } catch (Exception e) {
            throw new RepositoryException(e.getMessage(), e.getCause());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Object> getResultList(String querry, Map<String, Object> params) throws RepositoryException {
        try {
            EntityManager entityManager = dbConnection.getEntityManager();
            Query tq = entityManager.createNamedQuery(querry);
            params.forEach((key, value) -> tq.setParameter(key, value));
            return tq.getResultList();
        } catch (Exception e) {
            throw new RepositoryException(e.getMessage(), e.getCause());
        }

    }
}