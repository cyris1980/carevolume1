package ch.zbw.stcy.rentcar.infrastructure;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by cyrill on 26.05.17.
 */
public class DataBaseConnection implements IDataBaseConnection {

    private final EntityManagerFactory entityManagerFactory;

    public DataBaseConnection () throws RepositoryException {
        this("RentCar");
    }

    /**
     * @param dataBaseName
     * @throws RepositoryException
     */
    public DataBaseConnection (String dataBaseName) throws RepositoryException{
        try{
            entityManagerFactory = Persistence.createEntityManagerFactory(dataBaseName);
        }catch (Exception e) {
            throw new RepositoryException(e.getMessage(), e.getCause());
        }
    }

    public EntityManager getEntityManager() throws RepositoryException {
        try{
            return entityManagerFactory.createEntityManager();
        }catch (Exception e){
            throw new RepositoryException(e.getMessage(), e.getCause());
        }
    }
}
