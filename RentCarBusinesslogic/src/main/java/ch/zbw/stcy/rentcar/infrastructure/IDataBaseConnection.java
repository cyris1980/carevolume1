package ch.zbw.stcy.rentcar.infrastructure;

import javax.persistence.EntityManager;

/**
 * Created by cyrill on 26.05.17.
 */
public interface IDataBaseConnection {

    public EntityManager getEntityManager();
}
