package ch.zbw.stcy.rentcar.infrastructure.car;

import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClass;
import ch.zbw.stcy.rentcar.businesslogic.car.Car;
import ch.zbw.stcy.rentcar.businesslogic.customer.Customer;
import ch.zbw.stcy.rentcar.infrastructure.IRepository;
import ch.zbw.stcy.rentcar.infrastructure.carClass.CarClassQueries;
import ch.zbw.stcy.rentcar.infrastructure.customer.CustomerQueries;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by stefanallenspach on 07.07.17.
 */
public class CarController implements ICarController {

    private final IRepository<Car> repository;

    public CarController(IRepository<Car> repository) {
        this.repository = repository;
    }


    @Override
    public List<Car> getAllCars() {
        return repository.getEntityList(CarQueries.CAR_FIND_ALL);
    }

    @Override
    public List<Car> getAllCarsByCarClass(CarClass carClass) {
        Map<String, Object> params = new HashMap<>();
        params.put("carClass", carClass);
        return repository.getEntityList(CarQueries.CAR_FIND_BY_CAR_CLASS, params);
    }


    @Override
    public Optional<Car> getCarByIdentification(String identification) {
        Map<String, Object> params = new HashMap<>();
        params.put("ident", identification);
        return repository.getEntity(CarQueries.CAR_FIND_BY_IDENT, params);
    }

    @Override
    public Car saveCar(Car car) {
        return repository.saveEntity(car);
    }

    @Override
    public boolean deleteCarByIdentification(String identification) {
        Map<String, Object> params = new HashMap<>();
        params.put("ident", identification);
        Optional<Car> opCar = repository.getEntity(CarQueries.CAR_FIND_BY_IDENT, params);
        if (opCar.isPresent()) {
            repository.deleteEntity(opCar.get());
            return true;
        } else
            return false;
    }
}
