package ch.zbw.stcy.rentcar.businesslogic.customer;


import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by cyrill on 26.05.17.
 */
@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "STREET", length = 80)
    private String street;


    @Column(name = "POST_ZIP", length = 40)
    @NotNull
    @Length(min = 4, message = "min length 4 characters")
    private String postZip;

    @NotNull
    @Column(name = "CITY", length = 80)
    private String city;

    @NotNull
    @Column(name = "COUNTRY", length = 80)
    private String country;


    public Address() {
    }

    public Address(String street, String postZip, String city, String country) {
        setStreet(street);
        setPostZip(postZip);
        setCity(city);
        setCountry(country);
    }

    public Long getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostZip() {
        return postZip;
    }

    public void setPostZip(String postZip) {
        this.postZip = postZip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        if (street != null ? !street.equals(address.street) : address.street != null) return false;
        if (postZip != null ? !postZip.equals(address.postZip) : address.postZip != null) return false;
        if (city != null ? !city.equals(address.city) : address.city != null) return false;
        return country != null ? country.equals(address.country) : address.country == null;
    }

    @Override
    public int hashCode() {
        int result = street != null ? street.hashCode() : 0;
        result = 31 * result + (postZip != null ? postZip.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", street='" + street + '\'' +
                ", postZip='" + postZip + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
