package ch.zbw.stcy.rentcar.infrastructure.reservation;


/**
 * Created by stefanallenspach on 17.07.17.
 */
public class ReservationQueries {

    public final static String RESERVATION_FIND_ALL                 = "Reservation.findAll";
    public final static String RESERVATION_FIND_ALL_FROM_CUSTOMER   = "Reservation.findAllFromCustomer";
    public final static String RESERVATION_FIND_BY_NUMBER           = "Reservation.findByNumber";
    public final static String RESERVATION_FIND_BY_DATE             = "Reservation.findByDate";
    public final static String RESERVATION_DELETE_BY_NUMBER         = "Reservation.deleteByNumber";
    public final static String RESERVATION_FIND_NEXT_NUMBER         = "Reservation.findNextNumber";
}
