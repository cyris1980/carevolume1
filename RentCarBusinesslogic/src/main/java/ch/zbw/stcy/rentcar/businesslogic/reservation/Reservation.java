package ch.zbw.stcy.rentcar.businesslogic.reservation;

import ch.zbw.stcy.rentcar.businesslogic.car.Car;
import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClass;
import ch.zbw.stcy.rentcar.businesslogic.customer.Customer;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;

/**
 * Created by stefanallenspach on 07.07.17.
 */
@Entity
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    private long id;

    @Column(name = "RESERVATIONNUMBER")
    private long reservationNumber;

    @Column(name = "STARTDATE")
    private LocalDate startDate;

    @Column(name = "ENDDATE")
    private LocalDate endDate;

    @ManyToOne
    @JoinColumn(name = "CARCLASS_ID")
    private CarClass carClass;

    @ManyToOne
    @JoinColumn(name = "CAR_ID")
    private Car car;

    @ManyToOne
    @JoinColumn(name = "CUSTOMER_ID")
    private Customer customer;

    public Reservation() {
    }

    public Reservation(Customer customer, LocalDate startDate, LocalDate endDate, CarClass carClass) {
        this.customer = customer;
        this.startDate = startDate;
        this.endDate = endDate;
        this.carClass = carClass;
    }

    public long getId() {
        return id;
    }

    public long getReservationNumber() {
        return reservationNumber;
    }

    public void setReservationNumber(long reservationNumber) {
        if (this.reservationNumber != 0 && this.reservationNumber != reservationNumber) {
            throw new IllegalArgumentException("reservationNumber can not be overwritten");
        }
        this.reservationNumber = reservationNumber;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public CarClass getCarClass() {
        return carClass;
    }

    public void setCarClass(CarClass carClass) {
        this.carClass = carClass;
    }

    public Optional<Car> getCar() {
        return Optional.ofNullable(car);
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public BigDecimal getTotalCost() {
        int days = Period.between(startDate, endDate).getDays() + 1;
        return carClass.getDayRate().multiply(new BigDecimal(days));
    }

    public void update(Reservation other) {
        setReservationNumber(other.reservationNumber);
        setCustomer(other.customer);
        setStartDate(other.startDate);
        setEndDate(other.endDate);
        setCarClass(other.carClass);
        setCar(other.car);
    }

    public boolean isValid() throws IllegalArgumentException {
        StringBuilder errorMessage = new StringBuilder();

        if (startDate == null) {
            errorMessage.append("startDate is null;");
        }
        if (endDate == null) {
            errorMessage.append("endDate is null;");
        }
        if (startDate != null && endDate != null && endDate.isBefore(startDate)) {
            errorMessage.append("endDate is before startDate;");
        }
        if (customer == null) {
            errorMessage.append("customer is null;");
        }
        if (carClass == null) {
            errorMessage.append("carClass is null;");
        }
        if (errorMessage.length() > 0) {
            throw new IllegalArgumentException(errorMessage.toString());
        }

        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Reservation that = (Reservation) o;

        return new EqualsBuilder()
                .append(reservationNumber, that.reservationNumber)
                .append(startDate, that.startDate)
                .append(endDate, that.endDate)
                .append(carClass, that.carClass)
                .append(car, that.car)
                .append(customer, that.customer)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(reservationNumber)
                .append(startDate)
                .append(endDate)
                .append(carClass)
                .append(car)
                .append(customer)
                .toHashCode();
    }
}
