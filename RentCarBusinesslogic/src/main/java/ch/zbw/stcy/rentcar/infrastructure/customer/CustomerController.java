package ch.zbw.stcy.rentcar.infrastructure.customer;

import ch.zbw.stcy.rentcar.businesslogic.customer.Customer;
import ch.zbw.stcy.rentcar.infrastructure.IRepository;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * Created by cyrill on 26.05.17.
 */
public class CustomerController implements ICustomerController {

    private IRepository<Customer> repository;

    public CustomerController(IRepository<Customer> repository) {
        this.repository = repository;
    }


    /**
     * Speichert oder Updatet ein Customer auf der DB
     * @param customer
     * @return
     */
    @Override
    public Customer saveCustomer(Customer customer) {
        return repository.saveEntity(customer);
    }

    /**
     * gives all customers
     *
     * @return a List of Customers
     */
    @Override
    public List<Customer> getAllCustomers() {
        return repository.getEntityList(CustomerQueries.CUSTOMER_FIND_ALL);
    }

    @Override
    public List<Customer> searchCustomers(String searchString) {

        if (StringUtils.isNumeric(searchString)) {
            int number = Integer.parseInt(searchString);
            List<Customer> list = new ArrayList<>();
            Optional<Customer> oCustomer = getCustomerByNr(number);
            if (oCustomer.isPresent()) {
                list.add(getCustomerByNr(number).get());
                return list;
            }
            //Falls Jemand anstelle eines Namen eine Numer als Namen hat (z.B. 7)
        }

        Map<String, Object> params = new HashMap<>();
        params.put("searchString", searchString);
        return repository.getEntityList(CustomerQueries.CUSTOMER_SEARCH, params);

    }

    /**
     * returns an Optional the customer with the given number
     * or an empty Optional if the customer with the given numver
     * does not exists
     *
     * @param number from the customer
     * @return
     */
    @Override
    public Optional<Customer> getCustomerByNr(long number) {
        Map<String, Object> params = new HashMap<>();
        params.put("no", number);
        return repository.getEntity(CustomerQueries.CUSTOMER_FIND_BY_NUMBER, params);
    }

    /**
     * returns a list of customers with the given name
     *
     * @param name der gesuchten Custoemrs
     * @return Liste der Custoerm mit die den gegebenen namen haben
     */
    @Override
    public List<Customer> getCustomersByName(String name) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        return repository.getEntityList(CustomerQueries.CUSTOMER_FIND_BY_NAME, params);

    }

    @Override
    public boolean deleteCustomer(long customerNr) {
        Map<String, Object> params = new HashMap<>();
        params.put("no", customerNr);
        Optional<Customer> opCustomer = repository.getEntity(CustomerQueries.CUSTOMER_FIND_BY_NUMBER, params);
        if (opCustomer.isPresent()) {
            repository.deleteEntity(opCustomer.get());
            return true;
        } else
            return false;

    }
}
