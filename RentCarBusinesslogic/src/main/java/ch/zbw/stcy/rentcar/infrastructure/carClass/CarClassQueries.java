package ch.zbw.stcy.rentcar.infrastructure.carClass;

/**
 * Created by stefanallenspach on 09.07.17.
 */
public class CarClassQueries {

    public final static String CARCLASS_FIND_ALL        = "CarClass.findAll";
    public final static String CARCLASS_FIND_BY_NAME    = "CarClass.findByName";
    public final static String CARCLASS_DELETE_BY_NAME  = "CarClass.deleteByName";
}
