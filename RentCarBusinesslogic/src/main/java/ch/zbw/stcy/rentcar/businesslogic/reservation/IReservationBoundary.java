package ch.zbw.stcy.rentcar.businesslogic.reservation;

import ch.zbw.stcy.rentcar.businesslogic.customer.Customer;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Created by stefanallenspach on 09.07.17.
 */
public interface IReservationBoundary {
    Reservation saveReservation(Reservation reservation) throws IllegalArgumentException;

    Reservation updateReservation(Reservation reservation) throws IllegalArgumentException;

    List<Reservation> getAllReservation();

    List<Reservation> getAllReservationFromCustomer(Customer customer);

    Optional<Reservation> getReservationByNr(long reservationNumber);

    List<Reservation> getReservationByStartDate(LocalDate startDate);

    boolean deleteReservation(long reservationNr);
}
