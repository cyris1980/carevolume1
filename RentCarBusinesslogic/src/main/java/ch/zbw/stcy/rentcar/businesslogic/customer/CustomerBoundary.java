package ch.zbw.stcy.rentcar.businesslogic.customer;

import ch.zbw.stcy.rentcar.infrastructure.customer.ICustomerController;

import java.util.List;
import java.util.Optional;

/**
 * Created by cyrill on 26.05.17.
 */
public class CustomerBoundary implements ICustomerBoundary {
    private ICustomerController controller;

    public CustomerBoundary(ICustomerController customerController) {

        this.controller = customerController;
    }

    @Override
    public Customer saveCustomer(Customer customer) throws IllegalArgumentException{
        if(customer == null) throw new IllegalArgumentException("null not allowed");
        if(controller.getCustomerByNr(customer.getNumber()).isPresent()){
            throw new IllegalArgumentException("customer with no bereits in list");
        }else{
//            hack fuer die automatische generierung der Customer Nr.
            customer = controller.saveCustomer(customer);
            customer.setNumber(customer.getId());
            return controller.saveCustomer(customer);
        }
    }

    @Override
    public Customer updateCustomer(Customer customer) throws IllegalArgumentException {

        Customer customerDB = controller.getCustomerByNr(customer.getNumber())
                .orElseThrow(() -> new IllegalArgumentException("customer not in list"));

        customerDB.update(customer);
        return controller.saveCustomer(customerDB);
    }

    @Override
    public List<Customer> getAllCustomers() {
        return controller.getAllCustomers();
    }

    @Override
    public List<Customer> searchCustomers(String searchString) {
        return controller.searchCustomers(searchString);
    }

    @Override
    public Optional<Customer> getCustomerByNr(long number) {
        return controller.getCustomerByNr(number);
    }

    @Override
    public List<Customer> getCustomerByName(String name) {
        return controller.getCustomersByName(name);
    }

    @Override
    public boolean deleteCustomer(long customerNr) {
        return controller.deleteCustomer(customerNr);
    }
}
