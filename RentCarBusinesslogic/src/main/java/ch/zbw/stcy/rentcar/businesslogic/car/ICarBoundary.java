package ch.zbw.stcy.rentcar.businesslogic.car;

import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClass;

import java.util.List;
import java.util.Optional;

/**
 * Created by stefanallenspach on 07.07.17.
 */
public interface ICarBoundary {

    /**
     * Speichert einen noch nicht vorhanden Car in die DB
     *
     * @param car zu speichernen Car
     * @return gespeicherter Car
     * @throws IllegalArgumentException falls car mit identification bereits erfasst
     */
    Car saveCar(Car car) throws IllegalArgumentException;

    /**
     *
     * updaten einen bereits vorhandnen Car
     *
     * @param car mit den neuen Werten
     * @return upgedateter Car
     * @throws IllegalArgumentException falls car mit identnummer nicht in der DB vorhanden
     */
    Car updateCar(Car car) throws IllegalArgumentException;

    List<Car> getAllCars();

    List<Car> getAllCarsByCarClass(CarClass carClass);

    Optional<Car> getCarByIdentification(String identification);

    boolean deleteCarByIdentification(String identification);

}
