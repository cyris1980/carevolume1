package ch.zbw.stcy.rentcar.businesslogic.customer;

import ch.zbw.stcy.rentcar.businesslogic.customer.Customer;

import java.util.List;
import java.util.Optional;

/**
 * Created by cyrill on 26.05.17.
 */
public interface ICustomerBoundary {

    /**
     *
     * Speichert einen Customer.
     * @param customer
     * @return der gespeicherte Customer
     * @throws IllegalArgumentException Falls sich bereits ein Customer mit der gleichen CustomerNo vorhandne ist
     */
    Customer saveCustomer(Customer customer) throws IllegalArgumentException;

    /**
     *
     * Updaten einen Customer
     * @param customer
     * @return
     * @throws IllegalArgumentException Falls sich der Customer nicht in der DB befindet
     */
    Customer updateCustomer(Customer customer) throws IllegalArgumentException;

    List<Customer> getAllCustomers() throws IllegalArgumentException;

    List<Customer> searchCustomers(String searchString);

    Optional<Customer> getCustomerByNr(long number);

    List<Customer> getCustomerByName(String name);

    boolean deleteCustomer(long customerNr);
}
