package ch.zbw.stcy.rentcar.infrastructure;

/**
 * Created by cyrill on 26.05.17.
 */
public class RepositoryException extends RuntimeException {

    public RepositoryException(String message) {
        super(message);
    }

    public RepositoryException(String message, Throwable cause){
        super(message, cause);
    }


    public RepositoryException(Throwable cause) {
        super(cause);
    }
}
