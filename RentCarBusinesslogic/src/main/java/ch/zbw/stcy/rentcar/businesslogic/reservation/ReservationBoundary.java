package ch.zbw.stcy.rentcar.businesslogic.reservation;

import ch.zbw.stcy.rentcar.businesslogic.customer.Customer;
import ch.zbw.stcy.rentcar.infrastructure.reservation.IReservationController;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Created by stefanallenspach on 09.07.17.
 */
public class ReservationBoundary implements IReservationBoundary {

    private IReservationController controller;

    public ReservationBoundary(IReservationController reservationController) {
        this.controller = reservationController;
    }

    @Override
    public Reservation saveReservation(Reservation reservation) throws IllegalArgumentException {
        if (reservation == null) throw new IllegalArgumentException("reservation is null");
        reservation.isValid();
        if (controller.getReservationByNr(reservation.getReservationNumber()).isPresent()) {
            throw new IllegalArgumentException("reservation alredy in list");
        } else {
            //        hack wegen automatischer Reservationsnmmern vergabe
            Reservation savedReservation = controller.saveReservation(reservation);
            savedReservation.setReservationNumber(savedReservation.getId());
            savedReservation = controller.saveReservation(savedReservation);

            return savedReservation;
        }
    }

    @Override
    public Reservation updateReservation(Reservation reservation) throws IllegalArgumentException {
        if (reservation == null) throw new IllegalArgumentException("reservation is null");
        reservation.isValid();
        Reservation reservationDB = controller.getReservationByNr(reservation.getReservationNumber())
                .orElseThrow(() -> new IllegalArgumentException("reservation not in list"));

        reservationDB.update(reservation);
        return controller.saveReservation(reservationDB);
    }

    @Override
    public List<Reservation> getAllReservation() {
        return controller.getAllReservation();
    }

    @Override
    public List<Reservation> getAllReservationFromCustomer(Customer customer) {
        return controller.searchAllReservationFromCustomer(customer);
    }

    @Override
    public Optional<Reservation> getReservationByNr(long reservationNumber) {
        return controller.getReservationByNr(reservationNumber);
    }

    @Override
    public List<Reservation> getReservationByStartDate(LocalDate startDate) {
        return controller.getReservationByStartDate(startDate);
    }

    @Override
    public boolean deleteReservation(long reservationNr) {
        return controller.deleteReservation(reservationNr);
    }
}
