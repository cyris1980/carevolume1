package ch.zbw.stcy.rentcar.businesslogic;

import ch.zbw.stcy.rentcar.businesslogic.car.CarBoundary;
import ch.zbw.stcy.rentcar.businesslogic.car.ICarBoundary;
import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClassBoundary;
import ch.zbw.stcy.rentcar.businesslogic.carClass.ICarClassBoundary;
import ch.zbw.stcy.rentcar.businesslogic.customer.CustomerBoundary;
import ch.zbw.stcy.rentcar.businesslogic.customer.ICustomerBoundary;
import ch.zbw.stcy.rentcar.businesslogic.reservation.IReservationBoundary;
import ch.zbw.stcy.rentcar.businesslogic.reservation.ReservationBoundary;
import ch.zbw.stcy.rentcar.infrastructure.RepositoryFactory;

/**
 * Created by cyrill on 26.05.17.
 */
public class BoundaryFactory {

    private BoundaryFactory(){

    }

    public static ICustomerBoundary createCustomerBoundary(){
        return new CustomerBoundary(RepositoryFactory.createCustomerDBOperations());
    }

    public static ICarClassBoundary createCarClassBoundary() {
        return  new CarClassBoundary(RepositoryFactory.createCarClassDBOperations());
    }

    public static ICarBoundary createCarBoundary() {
        return  new CarBoundary(RepositoryFactory.createCarDBOperations());
    }

    public static IReservationBoundary createReservationBoundary(){
        return  new ReservationBoundary(RepositoryFactory.createReservationDBOperations());
    }
}
