package ch.zbw.stcy.rentcar.infrastructure.car;

import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClass;
import ch.zbw.stcy.rentcar.businesslogic.car.Car;

import java.util.List;
import java.util.Optional;

/**
 * Created by stefanallenspach on 07.07.17.
 */
public interface ICarController {

    List<Car> getAllCars();

    List<Car> getAllCarsByCarClass(CarClass carClass);

    Optional<Car> getCarByIdentification(String identification);

    Car saveCar(Car car);

    boolean deleteCarByIdentification(String identification);
}
