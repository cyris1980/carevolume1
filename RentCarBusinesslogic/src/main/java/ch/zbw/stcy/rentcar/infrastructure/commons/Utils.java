package ch.zbw.stcy.rentcar.infrastructure.commons;

import ch.zbw.stcy.rentcar.infrastructure.RentCarValidationException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Iterator;
import java.util.Set;

public class Utils {

    private static <E> Set<ConstraintViolation<E>> getConstraintValidations(E element) {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        return validator.validate(element);
    }

    public static <E> boolean validate(E element) throws RentCarValidationException {
        Set<ConstraintViolation<E>> constraintViolations = getConstraintValidations(element);
        Iterator<ConstraintViolation<E>> iterator = constraintViolations.iterator();
        if (iterator.hasNext()) {
            throw new RentCarValidationException(formatConstraintViolationsToErrorMessageGeneric(iterator));
        } else {
            return true;
        }
    }

    public static String formatConstraintViolationsToErrorMessage(Iterator<ConstraintViolation<?>> constraintViolationIterator) {
        StringBuilder errorMessage = new StringBuilder();
        while (constraintViolationIterator.hasNext()) {
            ConstraintViolation<?> next = constraintViolationIterator.next();
            String failMessage = next.getMessageTemplate() + " " + next.getRootBeanClass() + " " + next.getPropertyPath() + ";";
            errorMessage.append(failMessage);
        }
        return errorMessage.toString();
    }

    private static <E> String formatConstraintViolationsToErrorMessageGeneric(Iterator<ConstraintViolation<E>> constraintViolationIterator) {
        StringBuilder errorMessage = new StringBuilder();
        while (constraintViolationIterator.hasNext()) {
            ConstraintViolation<E> next = constraintViolationIterator.next();
            String failMessage = next.getMessageTemplate() + " " + next.getRootBeanClass() + " " + next.getPropertyPath() + ";";
            errorMessage.append(failMessage);
        }
        return errorMessage.toString();
    }
}
