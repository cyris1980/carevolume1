package ch.zbw.stcy.rentcar.businesslogic.customer;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by cyrill on 26.05.17.
 */

@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "NUMBER")
    private long number;

    @NotNull
    @Length(min = 2, message = "firstname minimum length is 2 characters")
    @Column(name = "FIRSTNAME")
    private String firstname;

    @NotNull
    @Length(min = 2, message = "name minimum length is 2 characters")
    @Column(name = "NAME")
    private String name;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "ADDRESS_ID")
    private Address address;

    public Customer() {
        address = new Address();
    }

    public Customer(String firstname, String name) {
        this();
        this.firstname = firstname;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        return new EqualsBuilder()
                .append(number, customer.number)
                .append(firstname, customer.firstname)
                .append(name, customer.name)
                .append(address, customer.address)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(number)
                .append(firstname)
                .append(name)
                .append(address)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", number=" + number +
                ", firstname='" + firstname + '\'' +
                ", name='" + name + '\'' +
                ", address=" + address +
                '}';
    }

    /**
     * Uebernimmt alle Werte des ohter Objekts
     * Wird gebraucht um das Objekt aus der Datenbank mit den neuen Werten upzudaten.
     * Ansonstent meusste die Tabellen id gesetzt werden. Diesr sollte aber nur von der DB gesetzt werden.
     *
     * @param other Objekt mit den neuen Werten
     */
    public void update(Customer other) {
        String firstname = other.getFirstname();
        String name = other.getName();
        this.firstname = firstname;
        this.name = name;
        this.address = other.getAddress();
    }
}
