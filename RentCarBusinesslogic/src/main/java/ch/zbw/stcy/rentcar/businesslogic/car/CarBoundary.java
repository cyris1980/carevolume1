package ch.zbw.stcy.rentcar.businesslogic.car;

import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClass;
import ch.zbw.stcy.rentcar.infrastructure.car.ICarController;
import ch.zbw.stcy.rentcar.infrastructure.customer.ICustomerController;

import java.util.List;
import java.util.Optional;

/**
 * Created by stefanallenspach on 07.07.17.
 */
public class CarBoundary implements ICarBoundary {
    private ICarController controller;

    public CarBoundary(ICarController carController) {
        this.controller = carController;
    }

    @Override
    public List<Car> getAllCars() {
        return controller.getAllCars();
    }

    @Override
    public List<Car> getAllCarsByCarClass(CarClass carClass) {
        return controller.getAllCarsByCarClass(carClass);
    }

    @Override
    public Optional<Car> getCarByIdentification(String identification) {
        return controller.getCarByIdentification(identification);
    }

    @Override
    public Car saveCar(Car car) throws IllegalArgumentException {
        if (car == null) throw new IllegalArgumentException("car is null");
        if (controller.getCarByIdentification(car.getIdentification()).isPresent()) {
            throw new IllegalArgumentException("car already in list");
        }
        car.isValid();
        return controller.saveCar(car);
    }

    @Override
    public Car updateCar(Car car) throws IllegalArgumentException {
        if (car == null) throw new IllegalArgumentException("car is null");
        Car carDB = controller.getCarByIdentification(car.getIdentification())
                .orElseThrow(() -> new IllegalArgumentException("car not in list"));

        carDB.update(car);
        carDB.isValid();
        return controller.saveCar(carDB);
    }

    @Override
    public boolean deleteCarByIdentification(String identification) {
        return controller.deleteCarByIdentification(identification);
    }
}
