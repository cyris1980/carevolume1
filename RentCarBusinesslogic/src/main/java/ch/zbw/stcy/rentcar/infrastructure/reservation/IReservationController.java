package ch.zbw.stcy.rentcar.infrastructure.reservation;

import ch.zbw.stcy.rentcar.businesslogic.customer.Customer;
import ch.zbw.stcy.rentcar.businesslogic.reservation.Reservation;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Created by stefanallenspach on 17.07.17.
 */
public interface IReservationController {
    public Reservation saveReservation(Reservation reservation);

    public List<Reservation> getAllReservation();

    public List<Reservation> searchAllReservationFromCustomer(Customer customer);

    public Optional<Reservation> getReservationByNr(long reservationNumber);

    public List<Reservation> getReservationByStartDate(LocalDate startDate);

    public boolean deleteReservation(long reservationNr);
}
