package ch.zbw.stcy.rentcar.infrastructure.carClass;

import ch.zbw.stcy.rentcar.businesslogic.carClass.CarClass;

import java.util.List;
import java.util.Optional;

/**
 * Created by stefanallenspach on 09.07.17.
 */
public interface ICarClassController {

    List<CarClass> getAllCarClasses();

    CarClass saveCarClass(CarClass carClass);

    boolean deleteCarClass(CarClass carClass);

    Optional<CarClass> getCarClassByName(String name);

    boolean deleteCarClassByName(String name);
}
